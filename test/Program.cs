﻿// See https://aka.ms/new-console-template for more information
using fastNpoi.Impl;
using fastNpoi.Interface;
using fastNpoi.Utils;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using test.Model;

Console.WriteLine("Hello, World!");
ServiceCollection serices = new ServiceCollection();
serices.AddScoped(typeof(IFastNpoi), typeof(FastNpoi));
var serviceProvider= serices.BuildServiceProvider();
var easyNpoi = serviceProvider.GetService<IFastNpoi>();
var list=new List<TestModel>(1000);
var list1=new List<TemplateModel>();
var item1 = FastNpoiUtils.Base64ToByte(@"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7muLeS0lZJF2MvUGm12F/psOpIFmU5Awrr95f8+h4rm9V0KbSiGP7yJukijg/X0P+ea/WqtFw16H6RwH4qYDP4rDYi1LE/wArekv8D6/4XqvPcNA8R33hXU47zT7iS3nTuOVcdwy9GHsf517f8NPjPZeOPLtLoR2OqHgRlsR3B/2Ce/8Asnn0zzXgmaOjZrzcVgadde9o+59bxHwpg83p/vVy1FtJbr17ryfyaPrAGivGfht8fptKEdjrzS3VrgKl4AXmhHAG8dXX3+9/vdvYbO9h1G0juLeWOe3mG6OSNgyuPUEV8vicLUoy5Z/efz5n3DeNymt7PEx0e0l8L9H37p6/IlooornPBCiiigAooooAKKKKACiiigDweggFSrDcjfeUjINFFfqh+OxlKLUouzRg6v4WwWktct/0zP3v+A+v86xQeK7iqOraFFqnzf6ufpu7P0xn/Hr9a46uH6wP6I4B8aalFxwHEDcobKrvJf419pea1736cr1roPAXxJ1L4fXf+iyCWzlbdNayH93J6kf3W/2h7ZyOKxbyxmsJvLlQq3b/AGvp614H+2h/wUh+Ev7Bul/8V54g36/NEJrXw3pardavdKfusYtwWGM9RJMyKwDbSxG2vMxEqSg/b2t5n9FYyvluLwPtMRKMqEle7aaa6NPv2tqfo14e1mPxDoVnfwqyR3kKTKpxldwzg47jp+Fcr8Xv2mvhn+z3eWtv8QPiR8PfANzfR+dbQ+JfEtjpEtwmSN6LcSoWXIIyARwa/E+P/gp7+3N/wU+8NW2gfATwvH8Efhjta1PippDDcTxliCTqcqbyUbHGnQiVOhLDmtD4R/8ABuN4T8Va1Nqfxm+JnjD4ieN9flWS5uLW+Swi85yqs0lxciWWfkj97IYhg8r3r4KtioqbVPY/muvlkPbz5Je5d29L6b+R+2Hwq+Nfgv48WUlz4E8aeD/HFvAN0kvh3XLXVo4x6s1vI4H4180/8Fav+CxPw+/4JR/DmyfWLd/FXxE8SQPL4f8AClrOIpJo1Yobu6kwfs9qHBUNtLyurLGpCSvF8F/ED/g1A8M2Zh1j4a/FLxT4R1Sxb7Qs8uo2GsNEV6mLyWtZFP0dj6Z6V6n+zX/wQD1a7/bej+Lnx3+Jlz8XvLsLG28NS6wnn3yvb28UMct58zK00SRbUU7i8jCRz5mc4vFSaJjliUrt3R4t4f8A+C0X/BSH4v2EPiLwz+z/AOBbbQ5F8yK3Xw7dKJkPIJFxfecSR/d25zwOlejfs/f8HVQ8DeNofBv7VHwW8RfCnxB8gn1PR7S48iLccB5dNusXMUQGTvjmnY9kNfotf6J4M8N+Itv2exENgDZC2nuHkjZkG0s7IwHmZzkKQoPAHFcv8fv2Z/hz+0v8Op9B8WfDfwn4x0e4ic28M0so+zOy4EsRkaQK3+1GY2A4Dg81mqs1rc6pYGk42t9x67+z9+0f4B/au+G1v4w+Gvi/QvG3hq4IT7dpdx5ggcqG8qZDiSCUAgmKVUkAIyorta/Af9oH/gk/8Uf+CavxEk+Lf7I/jvxN4V1SB/Lu/DFzOGkkhJ3GNXkBhvLfcB+4u0IyqnfK5UD7S/4JF/8ABwl4f/bd8XRfCn4waPZ/Cv43Rz/Ybe2fzLfTPEdwDtMMSzfPa3m4Ffs0jNvbHluWYRL008QpaS3PKxGBnD3oar8T9JqKOlFdJwHg9FT6npdzo169tdQyQzR/eVh+o9R7jioAc1+pRnGS5ou6Px2UXGTjLRoKFUyttXrTo4muJVjRSzuQoA6kmvx//wCCpn/BSPxh+3z8bbj9lv8AZvmkutHuJXsfF3iWzkbytVAYJNbxyICRp6M2ySRcm6ciNA0ZUXHDmWZU8HS9pPV9F3PUyfJ62Y1/ZUtFu30S/wA+yOu/4KM/8FxtX8deOJPgr+ynar4v8WXUpt77xfaxpcW1qV4caeW/dtt6NeyfuVUMU3ArMsH7Jf8AwQa8O/B3RP8Ahan7T/iKbxF4v1ovqNrpl2rX9xqM7/N5jQzEGXcxJae7+VgTiE4WU/Wv/BOv/gmJ4J/4Jc/Dqzt7XS7fxR8W9SVJbma6iR2tJcBkaUElfNB5jgyY4RhmLvl39W+FfwP1P4keN7nxh8UBNJqkzma20e/YmQgf8tJUY5KDsp4bGWyCAfzHG46ti6nPVfouiP3DLMDDB4aOEot8qd9XfV7vtrbocb8Nfh944+LGg2Vv4egj8I+ErSFYbe6uGPmPGOnl4G5h6FAidt3Fej/CD4KeFvhJ8R1nbVNY1rxCqtbtdzBUtwzABwFBLFsAj5mYfQitP9rv4t6x4KXw/wDD3wrePp/iDWtl/qdzEAJLWL/ljDnthQzt3AX3qPwz4kt/hf4O1/xjqVr/AGmdHs2itUdggmuZNibzwQD8+AcHBfPO3Fcd+x2aIb+23+15p/7HX7Nni/xhqFrYzW/h3TJdSlLr+9lPyxwW6MchHmnZIw+0lS+fSvxD/ZH/AOCunj79j2/8dftN+PPEWseNfiT8ZILrTfCvhS4vJF0bZFKEOpzxq37u3tZFaC3SMqx2TxqQpZ0+nv8Ag5C/aIuNW/YF0CKNW02b4h+I7NZrTf5mbS3t5J2XdgZHnLbNkjn0FfPPwD/4N9dU+J0nh+D4h/EPWNS1CS2jtbPRdEh+a0jwZPJW5uSRGqbnJUQhF+Zs4ya8vM84wmAgqmLnyp7bu9vQ6cPha2Ilakrngw/aj+OvhvU5f2gl/aD8Mr4+1B1afwqviGCWeTTwQyQtZAm28sHn7IQHXGdokyB+uX/BKb/guDoH7ZXgy38OzJZ6H44ghdtR8MXWXtJJOAbuwlJ37GbB2FtyHKPvBSSTzay/4NjPhr4a8KSzT+GdU1q4hTJil8TSteyeuBCscTN3wOvQAnAr5G/bj/4JX2P7Lvw9tvix8C59c0DxB8PZW1DUrWW7a4kNuhJe4j3jIaEZ8yNso8QfOChEnh5fxxlWMrKhRm7t2u0rX6devodtfI8VRg6kradv+GP3907SdL8e+FbaXXNPkbT9VgfynjdJGhO7axHZiCMFTgkdQM4r83v+CuX/AAQ00T9orSm1zw1Ja6X4tWLdo2tRIfI1JUAxbz4G7jgYIMkXBTco8psz4Bftp/ET/gpD/wAEqvFel/DPxhceA/iVf2qAXlndtayWeo2kkTzRxzg77fz48J5qsCI7iIsTtfPcf8EJf+Crt5+294I8RfBT4zRzp8VvBEZXUGnjWG41q1ikEJu9uAFvraQqkowPMDI2G/fbfrjyb3OM/wCCM/8AwXF8VeHPihZ/sy/tWT3Wm+PLOWPTPDfi7VpRv1RjgQ2l/MTiSR+BDd5PnZUSEsRI37BMrRsVZWVlOCCMEV+P/wDwW2/4JbW/7RHgu+ns7e1h8deHUL6ZqEYCpeBvmRGPTyZ8gcn93KytxmUN7X/wbjf8FM9U/bo/ZQvvBHjy8uLj4qfB1odK1Ga8c/bNZ01gUtLuUthnnQxSQTMcsTHFI7F5jXZh632ZHjY/C8v7yHzP0G8QeHLPxNaCG8j3Kv3JF4eP6H/IrzHxd4EvPCku9l8+zY4SdFO36EdVP6e55x65SGCO5HlyKjRyfK4ddykd8juPavdy/MquFlprHt/kfJZlktHFrm+Gff8AzPyC/wCC/X/BQfU/2afgrpfwl8BXE5+J/wAXkNov2P5rvTtKd/Jd4wOVluZN1vGQCcLcEFXVDXff8Ef/APgnto//AATO/ZZsPF2qwWM3xJ8XL9pW9kAMdoE4e4DHjZEWaOHPBPmS4BbavwppHivSPj7/AMHIvxAu/jQ9r4Ak8N6zPo/hzSNcufJija1aO0s4Vd8R7pLcPPGQQJJZVeMsWXMv/BK34reLv2pfiV+0R8Z/E3ijxFeR6pdWGiWely3btYJDLPLdRqsZOEFrDaQwwquAkc8igAGvIzXMJYzEOp02S8j6rh/K44DCRpL4nrJ92/0WyP1G/bz/AGhG/Yb/AOCZfxD+Ltu8beLb2xZfD91IFklhuLiRbS2mUtnJ86dp2670tucjNeJf8G4fwn8S6f8AsleHfEnjrxBrWtTeKNQv/G0zapdPPJZ2eVjRd7ljiYxeeTnB+09M7ieC/wCDq7Xf+Fd/8E0PhH4Jty0K3GvaRHIFON6Wuk3LMrDvmS4Rvqgr6k/ZJ0k/Bn/gmRqFvDuVtG8G6Z4cgcn5lDbYTz64Vfyrzra2Pc6lLwVr118YPil4s8eXSvNNq140NmpHKoduFH0jES8f7Vei/tx6evgD4O6P4VtJPOvr69gt5xkHzJFUyyhfYSmIfXNV/wBirwgtzP4TVoWkhtw+r3OBwQgacZ+u1Frhv+CgX7X/AMI/gP8AGj4bWXxM8bWXhnXdSnlvdHW6WQ28kpkiLPcOEaOGMOsfzysikk5JVWKtAtrn5uf8HG+hXnjDxX+zX8PdJmhjutQvNQtbfzifJ8yVtMtoWcBWO0EN0B4J4PSvdPhV+xZ/wU4+AujXXxQ8F+MvhN8ZP7One1n8OeVGLi/XCmQx+faWZ2jp+6uUkYhlUEnB8j/4LJXEep/8FKf2OZvMiuNIu9XsJYp43Dwzo+rWpLq4OGUqVORwQQQea/a7xp+3Z8M/+CZ3/BPfTPiV8UdVn0zQ/tMttbwWsH2i+1a8lmneO2t48jfKyRseSqqsbMzKqkjjxmAw2KhyYmnGa7SSe/qaUa9SlrTk16Ox8h/8Exv+C1Xhz9uXxxqHwt8ceGL74RfHjQfNjvvCeqLJEt/JCD9oFt5yrIssW1me1lUSooJUyqkrRx/8Fl/2nfhT+xF8L7bxh4qmjm8Va8zWun+GrdVa48UKAFkLqcrHGgZQ8zgja2zEj7Fr4w/4K7/tSfAv/gq38CpP2vv2a7vxB4F+O37Oup6bc+JbLU7OLT9autKlukgs9Q2xPNFcNbXfkqskcjFIpGWYAeQK9b/4IAfCK8/4LR/t8fEb9sr40aLYXtj4EmtfD3gnRCjT6XpV8sIkLRK7EZtI3SRdykGe/M42yIrV+by8M8NDNoYnDycaFm3FN3urWSf8r37qzs9Vb6CPEdV4V0pq8+j8u/qfHX/BvD46b4Pf8FDfE3w28ZeHtX8LaR4ihS7/ALI1F5YrrRS5WGEtuCsWSG+SUllG8QLkdBXbf8FhPhN4g/4Jpft2+Af2nvAFmlreaXrAs9fskykEtzEGikifAyIrm3823kIAAUrj5nzW3/wU9/4Js/GfwL+254+/aA+H/jzwT4Pj0rTY21KXU7x7W7sTZ2kVrcZV4JInDi3Dg5BYtjbuwW+P/HX/AAWi+M/7Znwx1r4b/EXw3oPxSfxPFM0l5aaY9prTzBWl89BbfuSY3UTf8e/WIHgA1+pbaHzXU/oqvvEvhn9r39lnw/458N3H9paJqmkRalYynHmzaZdKrbGAztlgkf5lPKEyDqvH5A/ArxPN/wAE1f8Ag4o8E6ov+ieF/jdKPD2tLHhIZH1CYWzlmPAVdQjtLxj6EdjXo/8AwbN/trTr+z18QPgR4uW6Gq/Du6l1XSLS6Xy5LaxuXKXdu6tgxrDdne2QTuvnB6ADiP8Ag400Szg+DHwi+Kfh6aFdU0DxVJZw39vwxeWBZkJPqjWIxnkc1V+pNSKnFxfU/fiiiivWPlD5H/4Kf/8ABJH4E/t/+GLrxd8RvD+pWvijwnpk1xFr+gXi2Go3FvBG0otZ2ZJEmi+XA3oZEBIjdATn8k/+CFGj+T+wB49vgv8Ax+ePI7djjr5WnKwH/kY/nX9DWt6Ha+KdDvtLvtwsdUt5LO4K/eEciFHx77WNfzB+BLD9rz/gmh8Fvi98MdO+DeqRaH4T12bWte8X3vhy7msdMESJB59vcvttXilWNHRmEnmKwKjBNceIik00exllTRxb9D6+/wCDs/xsvjP9nv4NtbzTXMUGv3ouJfK2Ju+xwLGB06Kj9vx619/+AdLvfHn7C+taDolr9uutSmt5YVWRVLiCdW2jOByNxHI5XHev5+Pj18Lfjl+1l+x3H8evH/xGvvGGk2bZ0/R5GkcWUAuWtJpFiVUgt8OikiJTuQ7mK4Ir78/Y8/4OFPg78G/2ZNO0nxNe+J214aPbWl9a2mitcSCcJF5xhkaRIwTIkmGLDKSkEA9PMw+Mo13L2Uublbi7dGt0e3Uo1KdnUVuZXXoeOf8ABcL9oOX4c/tefs5+E/EF9r2n+EvAmm6f4t1OPRJQt6bhrx43aH51XzkisgInLDYZnIIDHPhX/BUn/gob42/4KaadoXirUvhrd+D/AIT6Dq7QW2uf2d/aF5NPMMOpvWWNCSkJItoiibkUOzlUZY/21/20dJ/4KBft/eD/AB/4J8JeJLPQ9FsLPQ5RqMaSTS7bi4c3DrEXjhwtxgLvf/VFt3OB+u3/AATt8DeCP+Cg/wCzd4l8MfFbR18RXEdsNOvdPl/0e2uLN8iG6QR4ZZlaM4ZdojdI3UAlSPmuJuKo5PySnT5ot62eqV9dOr+aPrco4JxmYZVVzal8FOSTVn1taV9mrvldtU7H58Wf/BA74tXNx8N/F/w7+MnhTxN8JbeWHxJ4M1HWn1C3SCGaSO4Z4reOKWFXLKnmIsqMXQiRY2BA/V//AIKqf8Ette/4LG/8Ex/CHh/wFrOl6L44+Hmvz6lpsOqyPHp+qKfNjlt3dAxjkaORHSTYw3xlDtWQyJ8n/Dr4B/tnf8ECfF+q/wDCgYZf2kP2d9Tne7l8G3iGbVNL3OpO23jxKtwQWUy2ayRS4Ly26HYq9R8Z/wDg8C8bfAfwvFb6x+xn4s8Caxd+bBaJ4k1+eytVlUAttV9NjaXYXjLIuw4cZIyCfay3OMFmFJVsHUUk+z1+a3T9UfKYjC1aMnCrFpo+SfEv/BJbxN/wRB/4Jr/Hz4i/HPXfDlr46+MXh2L4Y+EfCul3v2wyfadQtby6uJ5Am0NHFYiRQhZcKwZgzoB9q/8ABlf8d9E0/wDZl+LvwVvmk03x94f8XP4sm027xDcS2VzZ2dpvSNsOfKls8SHGENxCDguM/GvxU/ZY/bX/AOCy3xm8P/GL40XXgnwZZ6IUm8OeGPEVvKunafbl1kKR6UqzMFkZV8z7YwklVEVy8aoBvfGH/gnz+294c/ayj/aO8I/Er4c6r8XLGKMS3mgRJod1q5RPLxNbyW8dlKGiCRusjBZFRAwJGa8StxxkVLFfVZ4umnqn72z00vbl73vJWtt29enwrms6Ht40J26adPTf8D94f+CmX7ENv/wUC/YW+Jnwji1CPQL/AMcabHDbals4huoJorm3MuBuaLzYIw4GTsLAc4r+e74If8EJviX/AMElfG2pftG/tIal4V8K+A/gu76npVvpetLd3/jfVVVxYWVoqDKJLceXvaUKwjDZQLveP6h+D3/B1h+1BrHxDk+EWq/so6b4l+NNsJEfTdMv7vTZCUj8wubKRJnKiL94Ss20p84IXmuX/aE/Zs/aA/ba1K1+OH7eWp2OjeC/DdxEvhP4N+G7sQwX9265WOdopJRBE4UtLI0s12YxKgMAWMD18xz3A4KmqleotVdJNNyvtyrrfvt1bscuVZHjsyxcMDg6TlUnJRSt1btZ9vmfnj+zT8WPi1+xP8c9D/au8XeE73WPCPxSvdR/twQsYjfw3krPKWHPlb5MTQeZ8shhGCV5r2jx98R7r/gtz+0r8H/2dfhWuuSeCl1X/hIfEeqS2vktYRtHGlxcSIcqq2luJQpLYkmuTGu7dGX9l/a6+Ies/tGfCPxfo981jpOm6npEtvFpOjafBY6dbiNA1vGkKqRtjeOMqTlhsGG4FfWP/BppB4a1f/gmPeatp/hnw/pview8W6j4e1fWbXTYYdR1iKNLa9gW4uFUSTLGL0qgdiFC4AGK5+F86eaU5SnHlaey7PVfPv0PrvFLw2zLgiWHhj5Rk60W1yttJppNO6W11tda7n6fKpdgqjLMcADvXwX/AMFD/wDg4L+EP7EPjG48B+F7HUfjR8Wo5ms28NeG5QbbT7hSFMN1dqsgWYHcDDDHNIrIyuIjivJP+C63/BTzxxafFKw/ZO/Z8vJLX4meKLRZ/F/iO3mMbeFtPljDi3SRctFM8JE0koG5IpIlj3STEJ8Gw/8ABG3wj4Z0TSZfC/jvxp4W8Y6XEFk1y1cFbiX+KQQqUePqQFSYYUAEucsfUzjiTCZfJUq07Sltpe3m7dD4Lhvg3G5rTeIpwvCPna+2iv1se1+NP29f+Chf7YMslxD4i8F/s7+GrofJYaXZoL/yzjB811ubpZAOo82DJydqjAHinx1/Y/8Ajv488C6nbeLv2pvi54+F9ZTRnTdQ1K/uLO8faWW3dZbxwUZsDlMdfl45fB+zt+114BH2fw/8bPC3iCzj4Rtat/NunAx95prSY/8AkU0y6+CH7ZXi+2a11D4sfD3S7eQqHEFnEsvUHKmOwz15+8OlfE4jPcbWl7SGMpRjfbXb0cWz9SwfDOV4enyTy+tKW13bf1UlH0uesf8ABLrxDD+0x/wQc8VeCd0MmpfDPXbiLySoleG0kuYtQ8wp2RxPeJz1Ecg7V4vZfsZeEVu1Y6P4bVgcjGixH+Zx/OuJ/ZL+Puu/8EVP2zLSfxZqFt4w8AfE7Tlj8WW1nGBI0RlYfaFgY8ywSM5XcAskck0Y2liU/SHxX+xHD8StCs/G3wX1rRvGXgfxEn2rTRBerkKTgrFK2FkVW3KVdlkQrsZSysa+Fz3EVMqxtTE0ZtYfEy54zXw8zS5ot9HdNq+6fdM/bfBHOOHFQlknE9KEa1J3i52stErX2WiTT23u9r/Jeh/BvRPDFvDGsBmSP7sbBUhXp0RQB+BzX3Z/wRt0C6n+J/jLVI42XTdP0aGxcgbUEss6PGuOnCwycdvxrynSP2A/iLdMs3iKx03wZpKn97qOrajAIo174EbtuOOgJUH1HWvoj4W/HD9n39lzwTD4aT4yfCaxkjYz3ct/4z0yG5vJyAGkZDMCOAAFA+VQBz1Pxuc5pUxFBwoKVWT/AJU3b5pM/VPFLjThyjkVTJsrq03OrZPlacYxUk9Wna+lkr+fr9nBcjgV+If/AAWG/au0n4Bf8F/PAPiD4vaPrOsfD3wB4Xsp/D1tbosy28kyTONQSCQhJGjvC+SCrZtYyCxiVD+h1p+318AruTbH8dPgrubGB/wnOlqSe2MzivH/ANvT9iD4Qf8ABWbwhpH9k/EDwzD8QPC4Y+HfEeg6pa6m8ALBzbTxRyHzrdm+YLkNG5LIcNIknLwbill2PbzWhUhSqQlBytJcvNbVOya7NrVJt+R/IeaYN1acZYOpGU4NSS0d7dN7feW/BX/BU39nP4iabHeaf8aPAsMci7gmqX39lzD1DJdCNgfqK574v/8ABZT9mv4MWLSXnxQ0jX7pVLR2fhyKTVpZsdg8S+Sp/wB+VRXxj4p/YU8Xfs23X2L43fsO+H/jLaxNsj8WfCq7vLB7lRjdLLp9kU2n/tharx0PJPXfC/XfAvhG4jh+C3/BOf4jX/ipmC29z4x8PNBZWEv8LteXn2jy9p5zviJxjevWvYlwHkal7SgqtaHRqpQUPnNyul3vFPyPVp8Y5lKHJUlTpy84VHL5RtZ/KTXmeC/tM/t5eLP2yf8Agof+zf8AEPwF8N9U8GWun+J7PSvBmrX0O3U/Fkg1C33JI6/I8KvKqCFDIqG4mBkfzCq/rN/wWmv5o9O+HtrGx/s97vUZSOoMii3VCforvj6muN/4J3/8E5fFsv7T0v7SH7TXiLQte+Ly2gsfC/hnTCW0bwJbFWG2NiSskyrJIihGdIy8shkmlkEkf1F+3f8As5/8NNfAO6t9PntYtY8POdX0+4mkVICUQiWOSU/KiPGW+ZiFDKhPANbZ5nGWyxGDweBUeShFxbjJyV5PZSdnJL+ayu27JI9PwxzJZXxRRzjNm+Rzu3JWsnFx5mlorN36tJdWfj/8V/F1v4A+FviPXLplWHS9Mnn543PsIRfqzFVHqWFfc3/Bp18MNR8Af8EoZtVvo2jh8ceOtV1nTyekltHBZ2O4f9t7O4X/AID7V+UvxAk8Rf8ABR/9ojQ/2f8A4T41C3vb0Ta/rcRE1jaQROvmTtIh2tawZ3lwSJZPLSMsSm/+lH9mf4UeFfgJ8APCHgPwSrR+F/BWlW+jWKyBRMViQAvNtAHnSNukcgAM8jN3r924EyOvhcG8TXi1zv7rLS/nu/uPL+lF4jZdxBxBSy7LqinDDxaundSlJpyce8VaKv1adrqzP5uPgF+xtL/wUV0XxV8dviB4q8T6L46+IPinUNWt7vS5UEKqZcu+xl3YExlRVV0CrEoHt3h/Zu/ak+AYEngj4s6f8RtNg6ab4jBW4lHZAbjeFHbi4Suz/YM+PPw18LfsnfD7w/J498E2OqWWlqLixudatre4imkkeV1KO4O7c54x3r6N065h1WzW7s5oLy1cfJPbyCWJunRlJB/OvhM3zzMIYypGtBSp8zSU4pqyduuuu+50ZFw3l0sDSlQm41OVXlCbTu0m9E7aPTVM+P2/4KZ+NvgjLHb/ABr+C/iDw3GrbH1fRgZLORuBhFlJjb/gNyfpXtXwe/bv+Efxwa3j0Xxxo8N9IVAsdUc6bc7jj5VE21ZG9o2evXHJEEi/wSAq6/wuD1BHcH0NeK/Fr/gnd8Hfjb5r6l4L0/S76X5ftuh/8S2YE4y22PETsfWSNua876xlWIfLVpOlLvB3XzjLb5M9b6nnGGX7itGtHtNcr/8AAo7/ADifnV/wVLnurn9s/VLeVZLqO3sLOG0i2n7jQq+FA55d3PHc1zHwN+Mvxg+BHhTUtF8H/Erxl4B0fWpFmvbHRdZuLT7Q+AN7LE6gMVAUkEEqNpyOK+ov2U/+CFPiL9tn4g/EK48K/ETTfCfhT4d66vh2zu9fjnvL66uEijlkCLbx7FRBIpBYpneoCn5iv0Fq3/Br38WntJm0D4v/AAf1q7X5khvrXULLd65MaSlf++T+Ffr2BwlP6jSou04qMd1vZKzs727+R+C5xia08wrVmuWTlK6TvbXVX6o/MPxF4eufHOqNqHiLXNe1/UJPv3N7eGaR/qz7m/WoU+HWixpj7EzH1ad+fyIr3z9tv/gnX8dv+CfKR3vxF8BH/hGZpFih8TaNd/2hozucAI8qLut3YkhUnWNn2sVDAE185yfEaRl+S1jXjOTIW/wruV0rLY8huT1ZqQfDjQ5QwaxbcFLZE7/41n6h8PtBi+80tp6ZuVH/AKEP85rLvvF19qAx5vkr3EPy/r1/Wszgn1J6n1pq4tT2T4VftFfFL4LqkPgL41+OvDMMJBS1sPENxBA3T5TGkoRh7FSPavoX4ef8Fyv2uvhnJCtx440HxxZw4222uaNaOXx/emjjhmOfUyn6818L7fap7PVbrT8eTcSxr/dDfL+XQ15eNyPL8Wv9qoQn/ijF/i0dtHMMVR/hVJR9G0frL8NP+Do/xTo1kE+I3wNsb6bHzXvhvWJbGEHHH7qeO4znj/lsPx6V8n/8FTv+Crf/AA8V8daPcaVfeO/BPgyz0qOyvPCk2o/arC7uUmkk+1bIzHGzsropMiMw8lcMRhV+YbH4h3ltKrSxwz7SCONjH8Rx+le3fsn/ALF/xc/4KCatcW/wv+GN/wCKreylEV5qlwYbbR7BwFJSS8n2xCTawYRBjIy8qrV5WXcE5LgMX9dwlBQqK+qbtr2TbS+SO3E8QY/EUfq9ed4/L80jzX9m/wDbS+IX7E3iy81j4R+Otd8LzaiI1vIRHHJb3yxkmNZ4JFeGYKWYjehxuOMZr+iz/gjl+2r8QP2v/wBi/Q/iV4y07StH1+81G809JNNheG11a2t3SMXBjdmALSCZGCnZuiJUL91fy/8Ajh/wbaeM/gz+yR48+InjX4nfDrT9W8D6Fca6nh3wzpcl8bzyU3tDJdSGAxsEDElI5V3DuMNX7Mfs16P4f0D9nL4e2vhXRrXw74bXw1p8um6Zb7jHYwyW0cioCxLMRvO52JZ2LMxLEk/q3CsZ1Kk6cn7ltY9Hc/KeOZUqVKnOMf3jeklukvPfqj8O/wDgn7+xr8F/2pf2S9J1jXPCS3XiWwu7rTNUvbfVLuGSSVJPMjJRZfLH7iWHomDg9812eq/8EhtD8I3h1L4Y/Ebx38P9YHKyC5FxH7KGi8mRRnuWf6HpXof7SXwkb/gkd/wVr8WeF76I6d8Ff2grhtf8K3zpss9Mu3lLNa7sbEEE0sluVzkQyWcrkA174UKNhlwfQ1/PvEuOzPLcfJQqPklqk9VbqrO60fTtY/qzhHB5Rm+V06kqUfaRVpNe7K62d42eq1v3ufF998cv2kf2JY/P+I2h6f8AFrwLb/67W9J+W8sowPvyMqKygDktPEQTwJR1r6S/Z8/ad8F/tNeDJNa8H6st2LVQ15YzgRX2msRkLNFk4zggOpZGIO1mwcehQM0YVlYqwHBFfIv7WH7Blx4U1uT4pfBC4/4RHxxpwL3Wk2QEdlrKEgOqR/cV2HDREeVIB0VuW82nWwWYe7XiqVR7SWkW30kunqvme9LD4/LP3mGlKtSW8JazS01hLeVv5X8mfOv/AAUijsfh/wDtB+HbfwjJqPhnXvElodT16902+nga+BfZEWRX2Bl8mY5ABJfJz1PPfBz9vL9oL9mjVIrzwZ8WvF11b25Df2XrN0dSsXGRkCC4MkYJ6bkCuB0IODXBfGP45XP7R/x//wCEg1HTW0W8sdJisZ7MltsE0QCy7Q3zBTIzkK3zLnBJIyavav1TKMPVw+CpUKr96KSet9fXy2PwviPG0sVmdbEYdJRlLSyt87ee78z92P8AgkZ/wWX0H/gp1ouvfC34meFdNt/FP9lMdY0qeP7Xo+v2DskMrIspZtm6RVkt5S5AdWV3AcR/mr8c/wBjr9nn9k79sb4w/B34oSfE3Rv+Ed16O78Iaz4fgs9QtodHvYI7q2hvIrjE0rRRyonmQuCzBwwGBXN/8EP7W5n/AOCwXw7js7hoVmtdX+0AEjz0OkXJ2n1y5RuehXPXFei/8HDOp29//wAFc/HUNvIskln4e0KG7A6xSmzSRVPuY3Rvowr1Oa8Tx+bTU4CT/gm98I/idk/Df9pL4S6rIxytp4wivvBd2enyKJhLE7duHUH2rH8V/wDBFr45aBZfbLP4d3vifSnOIdQ8M6taazbzDjkeRI7/AJqK8HB29CR24r9H/wDg3k/Ye0f9oLxp4u+IN1JrQ1X4earph0y2sdQNnbyyv5kjSXCphplXyo8IziNhvVlfcFqd3YlO7PliL/giz8VtGsIb/wAaR+FPhVpMy70vvG3jHTtJiK+pXc8oH1SpIf2Fv2cfhsM+Ov2mk168j5fS/h34auNaE2OuzUJhDbD2JBzX1R/wXe/4JheFP2atH0n4zeDUktbrxNr5tPE1jLqP24+fdK8kd5G7s0kSmSNlMZcqRNHsVAjbvzbZzINzMW9yab0K2Prj9mL9ij9n/wDb3/ag8F/Bn4T+FfipptveXL654q8Y+Lddtpr+00W1UtNDbWloi28ck8jRQpLJ5uxnUlSMkfeH/BTr/grd4N/4JaeGtF+BHwN8H6DJrmkWaR2miQl49J8PW7fMsl0EYSzTS58zyy4dxJ58shLqJPnb/g180Xd+2X8XtdVlWbSfhuLBPm+b99qVvNkD2NsOe2R618Tft4+LpfHn/BSX48anNlpLTxbf6VE2c4itZ/ssfP8A1zgUfQ0+bS4c2lzC/aR/bI+N37RwutR8e/ELXNfsWkE0/hyK8ktNF8odYxbW7RxgbcAlQGK5+fPNf0YfsN/tDeFv2qP2RvAPjjwZZx6ToGpaTFappSyGT+xZbcC3lsix+ZhC8bIrNgugR+jCv5phx2BGMEEZBFfox/wbJ/tVr8P/AIs+OvgDq115Vl4kQ+KfDCSvw13FGq3USDu8lqsch7AWL9zX0PDGO9ji/Zy2np8+h8fxjl7xGB9rHem7/Lr92/yP/9k=");
var item2= FastNpoiUtils.ByteToBase64(item1.image,item1.fileExtention);
for(int i=0; i < 5000; i++)
{
    TestModel item = new TestModel()
    {
        Id = i,
        Name = $"创建用的名称{i}",
        RandomNum = new Random().Next(),
        JsonStr="{\"name\":\"test\",\"value\":1,\"test\":\"99999\"}",
        Sex= new Random().Next(0,2)
        //Image = item2,
        //forumal= "DISPIMG(\"ID_FDCAB62732914F268F7B5294EC4C9BB5\",1)"
    };
    //if (i <= 50)
    //{
    //    TemplateModel templateModel = new TemplateModel()
    //    {
    //        Prop = $"{i}:prop",
    //        Prop1 = $"{i}:prop1",
    //        Prop2 = $"{i}:prop2"
    //    };
    //    list1.Add(templateModel);
    //}
    list.Add(item);
}
Stopwatch stopwatch = new Stopwatch();
stopwatch.Restart();
#region 模板导出
//easyNpoi.InitWorkbook(@"C:\Users\tengfei8771\Desktop\excel\20220922105953.xlsx")
//    .UseTemplateSheet()
//    //.SetData("test",list[0])
//    .SetDataList("test", list)
//    .SetDataList("template", list1)
//    .WriteToSheet()
//    .ToFile(dirPath: @"C:\Users\tengfei8771\Desktop\excel");
#endregion
#region json动态解析导出
//easyNpoi.CreateWorkbook()
//	.CreateSheet(typeof(TestModel))
//	.SetPageLimit(2000)
//	.SetDataList(list)
//	.SetDataHeight(300)
//	.SetIgnoreProperty("Name")
//	.SetIgnoreProperty("Id")
//	.SetJsonMapping("JsonStr", new Dictionary<string, string>()
//	{
//		{"json的姓名","name" },
//		{"json的value","value" },
//		{"虚拟列","test" }
//	})
//	.DataMapper(t =>
//	{
//		if (t.Id <= 1000)
//		{
//			t.Id = 1000;
//		}
//		if (t.Id <= 2000 && t.Id >= 1500)
//		{
//			t.Id = 1500;
//		}
//		if (t.Id > 2500 && t.Id < 3000)
//		{
//			t.Id = 2500;
//		}
//		if (t.Id > 3000 && t.Id <= 3200)
//		{
//			t.Id = 3200;
//		}

//	})
//	.WriteToSheet()
//	.ToFile(@"C:\Users\tengfei8771\Desktop\excel");

easyNpoi.CreateWorkbook()
    .CreateSheet<TestModel>()
    .SetPageLimit(2000)
    .SetDataList(list)
    .SetProperty2CellValue(t => t.Sex, new Dictionary<int, object>() { { 0, "男" }, { 1, "女" } })
    //.SetDataHeight(300)
    //.SetIgnoreProperty(t => t.Name)
    //.SetIgnoreProperty("Id")
    .SetJsonMapping(t => t.JsonStr, new Dictionary<string, string>()
    {
        {"json的姓名","name" },
        {"json的value","value" },
        {"虚拟列","test" }
    })
    .DataMapper(t =>
    {
        if (t.Id <= 500)
        {
            t.Id = 1000;
        }
        if (t.Id <= 2000 && t.Id >= 1500)
        {
            t.Id = 1500;
        }
        if (t.Id > 2500 && t.Id < 3000)
        {
            t.Id = 2500;
        }
        if (t.Id > 3000 && t.Id <= 3200)
        {
            t.Id = 3200;
        }

    })
    .AutoSizeColumn()
    .WriteToSheet()
    .ToFile(@"C:\Users\tengfei8771\Desktop\excel");
#endregion
#region 导入
//var importList = easyNpoi.InitWorkbook(@"C:\Users\tengfei8771\Desktop\excel1\20230307105123.xlsx")
//	.GetSheet(typeof(TestModel))
//	.ToList();
//var importList = easyNpoi.InitWorkbook(@"C:\Users\tengfei8771\Desktop\excel\20230822151351.xlsx")
//    .GetSheet<TestModel>()
//    .SetCellValue2Property(t=>t.Sex,new Dictionary<string, int>() { { "男", 0 }, { "女",1} })
//    .ToList();
#endregion
stopwatch.Stop();
Console.WriteLine($"反射导出用时 {stopwatch.Elapsed}");



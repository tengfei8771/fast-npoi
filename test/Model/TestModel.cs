﻿using fastNpoi.Attributes;
using fastNpoi.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test.Model
{
    public class TestModel
    {
        [Npoi(headerName:"主键",mergeCell:true,position:2,dataType:DataType.Number)]
        public int Id { get; set; }
        [Npoi(headerName: "名称",position:1)]
        public string? Name { get; set; }
        [Npoi(headerName: "随机数", dataType: DataType.Number)]
        public int RandomNum { get; set; }
        [Npoi(headerName: "创建日期",columnWidth:256*20)]
        public DateTime CreateTime { get; set; } = DateTime.Now;
        [Npoi(headerName: "图片",dataType:DataType.Image,columnWidth:30*256)]
        public string Image { get; set; }
        [Npoi(headerName:"公式列",dataType:DataType.Formula)]
        public string forumal { get; set; }
        [Npoi(headerName: "性别")]
        public int Sex { get; set; }

        public string JsonStr { get; set; }
    }
}

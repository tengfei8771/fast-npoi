﻿// See https://aka.ms/new-console-template for more information
using NPOI.OpenXmlFormats.Spreadsheet;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

Console.WriteLine("Hello, World!");
var workbook=new XSSFWorkbook();
var sheet = workbook.CreateSheet();
var row = sheet.CreateRow(0);
var cell=row.CreateCell(0);
cell.SetCellType(CellType.Formula);
cell.SetCellFormula("DISPIMG(\"ID_FDCAB62732914F268F7B5294EC4C9BB5\",1)");
if (!Directory.Exists(@"C:\Users\tengfei8771\Desktop\excel"))
{
    Directory.CreateDirectory(@"C:\Users\tengfei8771\Desktop\excel");
}

string text = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
while (File.Exists(Path.Combine(@"C:\Users\tengfei8771\Desktop\excel", text)))
{
    text = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
}

using FileStream stream = File.Open(Path.Combine(@"C:\Users\tengfei8771\Desktop\excel", text), FileMode.Create);
workbook.Write(stream);
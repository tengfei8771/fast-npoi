﻿using fastNpoi.Interface.Excel;
using fastNpoi.Model;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Interface
{
    public interface IFastNpoi
    {
        /// <summary>
        /// 根据文件路径加载一个workbook
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IFastWorkbook InitWorkbook(string path);
        /// <summary>
        /// 根据文件流加载一个workbook
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        IFastWorkbook InitWorkbook(Stream stream);
        /// <summary>
        /// 创建一个workbook
        /// </summary>
        /// <returns></returns>
        IFastWorkbook CreateWorkbook();


    }
}

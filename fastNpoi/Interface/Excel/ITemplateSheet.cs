﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fastNpoi.Interface.Excel
{
    public interface ITemplateSheet
    {
        /// <summary>
        /// 增加对象数据源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="paramName">对象的占位符名称</param>
        /// <param name="data">真实数据</param>
        /// <returns></returns>
        ITemplateSheet SetData<T>(string paramName,T data) where T:class,new();
        /// <summary>
        /// 增加list对象数据源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="paramName">list的占位符名称</param>
        /// <param name="dataList">真实数据</param>
        /// <returns></returns>
        ITemplateSheet SetDataList<T>(string paramName,IEnumerable<T> dataList) where T : class, new();
        /// <summary>
        /// 将数据源根据表达式写入sheet中
        /// </summary>
        /// <returns></returns>
        IFastWorkbook WriteToSheet();
    }
}

﻿using fastNpoi.Model;
using fastNpoi.Model.Enum;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace fastNpoi.Interface.Excel
{
    public interface IFastSheet<T> where T : class, new()
    {
        /// <summary>
        /// 数据赋值实现类替换
        /// </summary>
        /// <param name="operation">赋值取值实现类</param>
        /// <returns></returns>
        IFastSheet<T> SetDataOperation(IDataOperation operation);
        /// <summary>
        /// 设置表头行高度
        /// </summary>
        /// <param name="height"></param>
        /// <returns></returns>
        IFastSheet<T> SetHeaderHeight(short height);
        /// <summary>
        /// 设置数据行高度
        /// </summary>
        /// <param name="height"></param>
        /// <returns></returns>
        IFastSheet<T> SetDataHeight(short height);
        /// <summary>
        /// 设置表头索引位置 默认为0
        /// </summary>
        /// <param name="headerIndex"></param>
        /// <returns></returns>
        IFastSheet<T> SetHeaderIndex(int headerIndex);
        /// <summary>
        /// 设置列起始位置 默认为0
        /// </summary>
        /// <param name="columnStartIndex"></param>
        /// <returns></returns>
        IFastSheet<T> SetColumnStartIndex(int columnStartIndex);
        /// <summary>
        /// 每行间隔列数
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        IFastSheet<T> SetRowSkipNumber(int offset);
        /// <summary>
        /// 每列间隔列数
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        IFastSheet<T> SetColumnSkipNumber(int offset);
        /// <summary>
        /// 设置表头和数据之间的间隔行数 默认为1
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        IFastSheet<T> SetHeaderSkipNumber(int offset);
        /// <summary>
        /// 设置表头的样式
        /// </summary>
        /// <param name="styleFunc"></param>
        /// <returns></returns>
        IFastSheet<T> SetHeaderStyle(Func<IWorkbook, ICellStyle> styleFunc);
        /// <summary>
        /// 设置数据的样式
        /// </summary>
        /// <param name="styleFunc"></param>
        /// <returns></returns>
        IFastSheet<T> SetDataStyle(Func<IWorkbook, ICellStyle> styleFunc);
        /// <summary>
        /// 设置添加批注的单元格样式
        /// </summary>
        /// <param name="styleFunc"></param>
        /// <returns></returns>
        IFastSheet<T> SetCommentCellStyle(Func<IWorkbook, ICellStyle> styleFunc);
        /// <summary>
        /// 设置属性映射关系(导出时会自动将指定属性的值转为字典内对应的值)
        /// </summary>
        /// <typeparam name="TResult">属性的类型</typeparam>
        /// <param name="expression">属性的表达式</param>
        /// <param name="dictionary">映射的字典</param>
        /// <returns></returns>
        IFastSheet<T> SetProperty2CellValue<TResult>(Expression<Func<T, TResult>> expression, Dictionary<TResult, object> dictionary);
        /// <summary>
        /// 设置属性映射关系(导出时会自动将指定属性的值转为字典内对应的值)
        /// </summary>
        /// <param name="propertyName">属性名</param>
        /// <param name="dictionary">字典</param>
        /// <returns></returns>
        IFastSheet<T> SetProperty2CellValue(string propertyName, Dictionary<object, object> dictionary);
        /// <summary>
        /// 设置cell内获取的值转换关系(导入时自动将cell内获取的值转化为对应字典的value)
        /// </summary>
        /// <typeparam name="TResult">属性的类型</typeparam>
        /// <param name="expression">表达式</param>
        /// <param name="dictionary">转化字典</param>
        /// <returns></returns>
        IFastSheet<T> SetCellValue2Property<TResult>(Expression<Func<T, TResult>> expression, Dictionary<string, TResult> dictionary);
        /// <summary>
        /// 设置cell内获取的值转换关系(导入时自动将cell内获取的值转化为对应字典的value)
        /// </summary>
        /// <param name="propertyName">属性名</param>
        /// <param name="dictionary">转化字典</param>
        /// <returns></returns>
        IFastSheet<T> SetCellValue2Property(string propertyName, Dictionary<string, object> dictionary);
        ///// <summary>
        ///// 设置json字段映射规则
        ///// </summary>
        ///// <param name="columnExpression">json字段表达式</param>
        ///// <param name="headerName">表头名</param>
        ///// <param name="keyName">json字段key的名称</param>
        ///// <returns></returns>
        //IFastSheet<T> SetJsonMapping(Expression<Func<T, object>> columnExpression, string headerName,string keyName);
        /// <summary>
        /// 设置json字段映射规则
        /// </summary>
        /// <param name="jsonPropertyExpression">json字段表达式</param>
        /// <param name="map">映射规则(key为映射的表头名,value为Json字段的key)</param>
        /// <returns></returns>
        IFastSheet<T> SetJsonMapping(Expression<Func<T, object>> jsonPropertyExpression, Dictionary<string, string> map);
        ///// <summary>
        ///// 设置json字段映射规则
        ///// </summary>
        ///// <param name="columnName">json字段名称</param>
        ///// <param name="headerName">表头名</param>
        ///// <param name="keyName">json字段key的名称</param>
        ///// <returns></returns>
        //IFastSheet<T> SetJsonMapping(string columnName, string headerName, string keyName);
        /// <summary>
        /// 设置json字段映射规则
        /// </summary>
        /// <param name="jsonPropertyName">json字段名称</param>
        /// <param name="map">映射规则(key为映射的表头名,value为Json字段的key)</param>
        /// <returns></returns>
        IFastSheet<T> SetJsonMapping(string jsonPropertyName, Dictionary<string, string> map);
        /// <summary>
        /// 设置批注的默认可见性
        /// </summary>
        /// <param name="visible">可见性 true可见 flase不可见</param>
        /// <returns></returns>
        IFastSheet<T> SetCommentVisable(bool visible);
        /// <summary>
        /// 设置实体list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        IFastSheet<T> SetDataList(List<T> list);
        /// <summary>
        /// 设置当前sheet页做多录入多少条数据
        /// </summary>
        /// <param name="pageLimt">每页多少条数据</param>
        /// <returns></returns>
        IFastSheet<T> SetPageLimit(int pageLimt);
        /// <summary>
        /// 设置表头映射规则(此规则优先级高于实体上的特性)
        /// </summary>
        /// <param name="headerName">表头名</param>
        /// <param name="propertyExpression">映射属性表达式</param>
        /// <param name="width">表格宽度</param>
        /// <param name="operationType">表头类型(导入,导出，导入和导出)</param>
        /// <param name="position">生成表头位置</param>
        /// <param name="mergeCell">是否合并单元格</param>
        /// <returns></returns>
        IFastSheet<T> HeaderMapper(string headerName, Expression<Func<T, object>> propertyExpression, int width = 0, OperationType operationType = OperationType.Import_Export, int position = 999, bool mergeCell = false,DataType dataType= DataType.Text);
        /// <summary>
        /// 设置表头映射规则(此规则优先级高于实体上的特性)
        /// </summary>
        /// <param name="headerName">表头名</param>
        /// <param name="propertyName">映射的属性名称</param>
        /// <param name="width">表格宽度</param>
        /// <param name="operationType">表头类型(导入,导出，导入和导出)</param>
        /// <param name="position">生成表头位置</param>
        /// <param name="mergeCell">是否合并单元格</param>
        /// <returns></returns>
        IFastSheet<T> HeaderMapper(string headerName, string propertyName, int width = 0, OperationType operationType = OperationType.Import_Export, int position = 999, bool mergeCell = false, DataType dataType = DataType.Text);
        /// <summary>
        /// 数据操作
        /// </summary>
        /// <param name="action">数据的操作委托</param>
        /// <returns></returns>
        IFastSheet<T> DataMapper(Action<T> action);
        /// <summary>
        /// 忽略数据中某一属性(及时标注特性也忽略此列)
        /// </summary>
        /// <param name="propertyExpression">属性表达式</param>
        /// <returns></returns>
        IFastSheet<T> SetIgnoreProperty(Expression<Func<T, object>> propertyExpression);
        /// <summary>
        /// 忽略数据中某一属性(及时标注特性也忽略此列)
        /// </summary>
        /// <param name="propertyName">属性的字符串名称</param>
        /// <returns></returns>
        IFastSheet<T> SetIgnoreProperty(string propertyName);
        /// <summary>
        /// 是否使用自适应宽度
        /// </summary>
        /// <param name="autoSizeColumn">是或否</param>
        /// <returns></returns>
        IFastSheet<T> AutoSizeColumn(bool autoSizeColumn=true);
        /// <summary>
        /// 表格合并
        /// </summary>
        /// <param name="startRowIndex">起始行序数</param>
        /// <param name="endRowIndex">结束行序数</param>
        /// <param name="startColumnIndex">起始列序数</param>
        /// <param name="endColumnIndex">结束列序数</param>
        /// <param name="sheetIndex">应用于哪个sheet,默认-1为全体sheet</param>
        /// <returns></returns>
        IFastSheet<T> MergeCell(int startRowIndex, int endRowIndex, int startColumnIndex, int endColumnIndex, int sheetIndex = -1);
        /// <summary>
        /// 使用CommentModel模型对指定cell添加批注
        /// </summary>
        /// <param name="commentModels"></param>
        /// <returns></returns>
        IFastSheet<T> CommentCell(List<CommentModel> commentModels);
        /// <summary>
        /// 对sheet添加批注
        /// </summary>
        /// <param name="func">添加批注操作 入参是表头的坐标,返回值为生成的评论模型</param>
        /// <returns></returns>
        IFastSheet<T> CommentCell(Func<Dictionary<string, int>, List<CommentModel>> func);
        /// <summary>
        /// 对sheet进行追加操作
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        IFastSheet<T> SheetMapper(Action<ISheet> action);
        /// <summary>
        /// 使特性中的position属性失效
        /// </summary>
        /// <returns></returns>
        IFastSheet<T> DisableOrderby();
        /// <summary>
        /// 把sheet写入workbook
        /// </summary>
        /// <returns></returns>
        IFastWorkbook WriteToSheet();
        /// <summary>
        /// 把sheet数据转为list
        /// </summary>
        /// <returns></returns>
        List<T> ToList();
    }
}

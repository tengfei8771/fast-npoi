﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace fastNpoi.Interface.Excel
{
    public interface IFastWorkbook
    {
        /// <summary>
        /// 内部维护的Workbook
        /// </summary>
        IWorkbook Workbook { get; }
        /// <summary>
        /// 创建一个sheet
        /// </summary>
        /// <param name="name">sheet名</param>
        /// <returns></returns>
        IFastSheet<T> CreateSheet<T>(string name) where T : class, new();
        /// <summary>
        /// 创建一个sheet
        /// </summary>
        /// <param name="name">sheet名</param>
        /// <param name="type">实体类型</param>
        /// <returns></returns>
        IFastDynamicSheet CreateSheet(Type type,string name);
		/// <summary>
		/// 创建一个sheet
		/// </summary>
		/// <returns></returns>
		IFastSheet<T> CreateSheet<T>() where T : class, new();
        /// <summary>
        /// 创建一个sheet
        /// </summary>
        /// <param name="type">实体类型</param>
        /// <returns></returns>
		IFastDynamicSheet CreateSheet(Type type);
		/// <summary>
		/// 根据索引读取sheet页
		/// </summary>
		/// <param name="index">sheet页索引</param>
		/// <returns></returns>
		IFastSheet<T> GetSheet<T>(int index = 0) where T : class, new();
		/// <summary>
		/// 根据索引读取sheet页
		/// </summary>
		/// <param name="type">实体类型</param>
		/// <param name="index">sheet页索引</param>
		/// <returns></returns>
		IFastDynamicSheet GetSheet(Type type, int index = 0);
		/// <summary>
		/// 根据sheet页名称读取sheet页
		/// </summary>
		/// <param name="name">sheet名</param>
		/// <returns></returns>
		IFastSheet<T> GetSheet<T>(string name) where T : class, new();
        /// <summary>
        /// 根据sheet页名称读取数据
        /// </summary>
        /// <param name="type">实体类型</param>
        /// <param name="name">sheet名</param>
        /// <returns></returns>
        IFastDynamicSheet GetSheet(Type type, string name);
		/// <summary>
		/// 使用模板方式导出数据
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		ITemplateSheet UseTemplateSheet(int index = 0);
        /// <summary>
        /// 使用模板方式导出数据
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ITemplateSheet UseTemplateSheet(string name);
        /// <summary>
        /// 将workbook写入文件
        /// </summary>
        /// <param name="dirPath"></param>
        /// <returns></returns>
        string ToFile(string dirPath);
        /// <summary>
        /// 将workbook转化为byte数组
        /// </summary>
        /// <returns></returns>
        byte[] ToByte();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Interface
{
    public interface IDataOperation
    {
        object GetValue<T>(T data, PropertyInfo property) where T : class;
        void SetValue<T>(T data, PropertyInfo property, object value) where T : class;
    }
}

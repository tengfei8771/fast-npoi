﻿using fastNpoi.Attributes;
using fastNpoi.Model;
using fastNpoi.Model.Enum;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Eval;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fastNpoi.Utils
{
    /// <summary>
    /// 工具类
    /// </summary>
    public class FastNpoiUtils
    {
        /// <summary>
        /// 获取实体的字段映射数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="operationType">操作烈性</param>
        /// <param name="IsOrderby">是否排序</param>
        /// <param name="ExtraInfo">使用代码添加的额外信息</param>
        /// <param name="IgnorePropertyinfos">被忽略的属性信息</param>
        /// <returns></returns>
        public static Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> GetHeaderAttrDic<T>(OperationType operationType,bool IsOrderby, Dictionary<PropertyInfo, NpoiAttribute> ExtraInfo,List<PropertyInfo> IgnorePropertyinfos)
            where T : class,new()
        {
            Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> result = new Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)>();
            Type type = typeof(T);
            IEnumerable<PropertyInfo> propertyInfos = type.GetProperties().Except(IgnorePropertyinfos).Where(t =>
            {
                if (ExtraInfo.TryGetValue(t, out NpoiAttribute dicAttr))
                {
                    return dicAttr.OperationType == operationType || dicAttr.OperationType == OperationType.Import_Export;
                }
                else
                {
                    var attr = t.GetCustomAttribute<NpoiAttribute>();
                    return attr != null && (attr.OperationType == operationType || attr.OperationType == OperationType.Import_Export);
                }
            });
            if (IsOrderby)
            {
                propertyInfos = propertyInfos.OrderBy(t =>
                {
                    if (ExtraInfo.TryGetValue(t, out NpoiAttribute dicAttr))
                    {
                        return dicAttr.Position;
                    }
                    else
                    {
                        return t.GetCustomAttribute<NpoiAttribute>()?.Position;
                    }
                });
            }
            foreach (PropertyInfo prop in propertyInfos)
            {
                NpoiAttribute attr;
                if (!ExtraInfo.TryGetValue(prop, out attr))
                {
                    attr = prop.GetCustomAttribute<NpoiAttribute>();
                }
                result.Add(attr.HeaderName, (prop, attr));

            }
            return result;
        }
        /// <summary>
        /// 根据exp表达式获取propertyName
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        public static object GetExpPropertyName(Expression expression)
        {
            object value = "";
            if (expression is UnaryExpression)
            {
                value = ((MemberExpression)((UnaryExpression)expression).Operand).Member.Name;
            }
            else if (expression is MemberExpression)
            {
                value = ((MemberExpression)expression).Member.Name;
            }
            else if (expression is ParameterExpression)
            {
                value = ((ParameterExpression)expression).Type.Name;
            }
            else if (expression is ConstantExpression)
            {
                value = ((ConstantExpression)expression).Value;
            }
            return value;
        }
        /// <summary>
        /// 从cell取值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static object GetCellValue(ICell cell)
        {
            if (cell == null) return "";
            object value = null;
            switch (cell.CellType)
            {
                case CellType.Numeric:    //用于取出数值和公式类型的数据 
                    value = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString("yyyy-MM-dd") : cell.NumericCellValue.ToString();
                    break;
                case CellType.Error:
                    value = ErrorEval.GetText(cell.ErrorCellValue);
                    break;
                case CellType.Formula:
                    switch (cell.CachedFormulaResultType)
                    {
                        case CellType.String:
                            string strFORMULA = cell.StringCellValue;
                            if (strFORMULA != null && strFORMULA.Length > 0)
                            {
                                value = strFORMULA.ToString();
                            }
                            break;
                        case CellType.Numeric:
                            value = Convert.ToString(cell.NumericCellValue);
                            break;
                        case CellType.Boolean:
                            value = Convert.ToString(cell.BooleanCellValue);
                            break;
                        case CellType.Error:
                            value = ErrorEval.GetText(cell.ErrorCellValue);
                            break;
                        default:
                            value = "";
                            break;
                    }
                    break;
                case CellType.Boolean:
                    // Boolean type
                    value = cell.BooleanCellValue.ToString();
                    break;

                case CellType.Blank:
                    break;

                default:
                    // String type
                    value = cell.StringCellValue.Trim();
                    break;
            }
            return value;
        }
        /// <summary>
        /// 赋值
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="value"></param>
        public static void SetCellValue(ICell cell,object value)
        {
            if (value.GetType() == typeof(string))
            {
                cell.SetCellValue(value.ToString());
            }
            else if(value.GetType() == typeof(DateTime))
            {
                cell.SetCellValue(((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else if(value.GetType() == typeof(int) ||
                value.GetType() == typeof(long) ||
                value.GetType() == typeof(double)||
                value.GetType() == typeof(float)||
                value.GetType() == typeof(decimal))
            {
                cell.SetCellValue(Convert.ToDouble(value));
            }
            else if (value.GetType().IsClass)
            {
                cell.SetCellValue(JsonConvert.SerializeObject(value));
            }
        }
        /// <summary>
        /// 根据datatype对数据进行赋值
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="dataType"></param>
        /// <param name="value"></param>
        public static void SetCellValue(ICell cell,DataType dataType,object value)
        {
            switch (dataType)
            {
                case DataType.Text:
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(value.ToString());
                    break;
                case DataType.Number:
                    cell.SetCellType(CellType.Numeric);
                    cell.SetCellValue(Convert.ToDouble(value));
                    break;
                case DataType.Formula:
                    cell.SetCellType(CellType.Formula);
                    //XSSFCell cell1= (XSSFCell)cell;
                    //cell1.SetCellFormula()
                    cell.SetCellFormula(value.ToString());
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 创建浮动图片,随单元格变化而变化
        /// </summary>
        /// <param name="workbook">工作簿</param>
        /// <param name="sheet">sheet</param>
        /// <param name="base64Str">图片的base64字符串</param>
        /// <param name="rowIndex">单元格行</param>
        /// <param name="columnIndex">单元格列</param>
        public static void CreatePictureCell(IWorkbook workbook,ISheet sheet,string base64Str, int rowIndex,int columnIndex)
        {
            var item= Base64ToByte(base64Str);
            int picId = 0;
            if (item.fileExtention == "png")
            {
                picId = workbook.AddPicture(item.image, PictureType.PNG);
            }
            else
            {
                picId = workbook.AddPicture(item.image, PictureType.JPEG);
            }
            IDrawing draw = sheet.DrawingPatriarch;
            if (draw == null)
            {
                draw = sheet.CreateDrawingPatriarch();
            }
            var anchor = draw.CreateAnchor(0, 0, 0, 0,columnIndex,rowIndex,columnIndex+1,rowIndex+1);
            anchor.AnchorType = AnchorType.MoveAndResize;
            draw.CreatePicture(anchor, picId);
        }
        /// <summary>
        /// base64图片转成byte[]
        /// </summary>
        /// <param name="base64Str"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static (byte[] image,string fileExtention) Base64ToByte(string base64Str)
        {
            if(!base64Str.Contains("data:")|| !base64Str.Contains(";base64,"))
            {
                throw new ArgumentException("无效的base64参数！");
            }
            string[] strArr = base64Str.Split(';');
            string[] fileInfo = strArr[0].Replace("data:", "").Split('/');
            if (fileInfo[0]!= "image")
            {
                throw new ArgumentException("不支持非图片的base64参数！");
            }
            byte[] image = Convert.FromBase64String(base64Str.Split(',')[1]);
            return (image, fileInfo[1]);
        }
        /// <summary>
        /// byte[]数组转base64字符串
        /// </summary>
        /// <param name="image"></param>
        /// <param name="fileExtention"></param>
        /// <returns></returns>
        public static string ByteToBase64(byte[] image,string fileExtention)
        {
            string originBase64=Convert.ToBase64String(image);
            return $"data:image/{fileExtention};base64,{originBase64}";
        }
        public static List<PictureAnchor> GetPictureAnchors(ISheet sheet)
        {
            List<PictureAnchor> pictureAnchors = new List<PictureAnchor>();
            IDrawing dp= sheet.DrawingPatriarch;
            if (dp == null)
            {
                return pictureAnchors;
            }
            List<IPicture> pictures = new List<IPicture>();
            if (dp is HSSFPatriarch patriarch)
            {
                pictures = patriarch.GetShapes().Select(x =>
                {
                    if (x is HSSFPicture picture)
                    {
                        return picture;
                    }
                    else
                    {
                        return new HSSFPicture(x, x.Anchor);
                    }
                }).Cast<IPicture>().ToList();
            }
            else if (dp is XSSFDrawing dr)
            {
                //TODO: How convert XSSFSimpleShape to XSSFPicture ???
                pictures = dr.GetShapes().Where(x => x is XSSFPicture).Cast<IPicture>().ToList();
            }
            else
            {
                return pictureAnchors;
            }
            foreach(var picture in pictures.Where(t=>t.PictureData.MimeType.StartsWith("image/")))
            {
                var anthor = picture.ClientAnchor;
                pictureAnchors.Add(new PictureAnchor(anthor.Row1, anthor.Col1, anthor.Row2, anthor.Col2, picture.PictureData.Data, picture.PictureData.MimeType.Split('/')[1]));
            }
            return pictureAnchors;
        }

        /// <summary>
        /// 获取正则表达式内的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string GetRegValue(string str, string begin, string end)
        {
            Regex rg = new Regex("(?<=(" + begin + "))[.\\s\\S]*?(?=(" + end + "))", RegexOptions.Multiline | RegexOptions.Singleline);
            return rg.Match(str).Value;
        }
        /// <summary>
        /// 通过反射生成泛型类
        /// </summary>
        /// <param name="baseGenericType">base泛型</param>
        /// <param name="typeArgments">泛型参数</param>
        /// <param name="constructorParamters">构造函数参数</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static object GetGenericInstance(Type baseGenericType, Type[] typeArgments, object[] constructorParamters)
        {
            if (!baseGenericType.IsGenericType)
            {
                throw new ArgumentException("baseGenericType must be GenericType!!");
            }
            var realType = baseGenericType.MakeGenericType(typeArgments);
            return Activator.CreateInstance(realType, constructorParamters);

		}
    }
}

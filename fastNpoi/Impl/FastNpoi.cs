﻿using fastNpoi.Attributes;
using fastNpoi.Impl.Excel;
using fastNpoi.Interface;
using fastNpoi.Interface.Excel;
using fastNpoi.Model;
using fastNpoi.Utils;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Eval;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace fastNpoi.Impl
{
    public class FastNpoi : IFastNpoi
    {

        public IFastWorkbook CreateWorkbook()
        {
            return new FastWorkbook(new XSSFWorkbook());
        }

        public IFastWorkbook InitWorkbook(string path)
        {
            return new FastWorkbook(WorkbookFactory.Create(path));
        }

        public IFastWorkbook InitWorkbook(Stream stream)
        {
            return new FastWorkbook(WorkbookFactory.Create(stream));
        }
        
    }
}

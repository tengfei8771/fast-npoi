﻿using fastNpoi.Interface;
using fastNpoi.Interface.Excel;
using fastNpoi.Utils;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace fastNpoi.Impl.Excel
{
    public class FastWorkbook: IFastWorkbook
    {
        public IWorkbook Workbook { get => _workbook; }
        private IWorkbook _workbook;
        public FastWorkbook(IWorkbook workbook)
        {
            _workbook=workbook;
        }
        public IFastSheet<T> CreateSheet<T>(string name) where T : class, new()
        {
            var sheet = _workbook.CreateSheet(name);
            return new FastSheet<T>(this, sheet);
        }

        public IFastSheet<T> CreateSheet<T>() where T : class, new()
        {
            ISheet sheet = _workbook.CreateSheet();
            return new FastSheet<T>(this, sheet);
        }
        public IFastSheet<T> GetSheet<T>(int index = 0) where T : class, new()
        {
            var sheet = _workbook.GetSheetAt(index);
            return new FastSheet<T>(this, sheet);
        }

        public IFastSheet<T> GetSheet<T>(string name) where T : class, new()
        {
            var sheet = _workbook.GetSheet(name);
            return new FastSheet<T>(this, sheet);
        }

        public byte[] ToByte()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                _workbook.Write(memoryStream,false);
                return memoryStream.ToArray();
            }
        }

        public string ToFile(string dirPath)
        {
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            string text = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            while (File.Exists(Path.Combine(dirPath, text)))
            {
                text = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            }

            using FileStream stream = File.Open(Path.Combine(dirPath, text), FileMode.Create);
            _workbook.Write(stream,false);
            return text;
        }

        public ITemplateSheet UseTemplateSheet(int index = 0)
        {
            return new TemplateSheet(this, _workbook.GetSheetAt(index));
        }

        public ITemplateSheet UseTemplateSheet(string name)
        {
            return new TemplateSheet(this, _workbook.GetSheet(name));
        }

		public IFastDynamicSheet CreateSheet(Type type, string name)
		{
            ValidateType(type);
			ISheet sheet = _workbook.CreateSheet(name);
            var fastWorkSheet = FastNpoiUtils.GetGenericInstance(typeof(FastSheet<>), new Type[1] { type }, new object[2] { this, sheet });
            return new FastDynamicSheet(fastWorkSheet,type);
		}

		public IFastDynamicSheet CreateSheet(Type type)
		{
			ValidateType(type);
			ISheet sheet = _workbook.CreateSheet();
			var fastWorkSheet = FastNpoiUtils.GetGenericInstance(typeof(FastSheet<>), new Type[1] { type }, new object[2] { this, sheet });
			return new FastDynamicSheet(fastWorkSheet,type);
		}

		public IFastDynamicSheet GetSheet(Type type, int index = 0)
		{
			ValidateType(type);
			ISheet sheet = _workbook.GetSheetAt(index);
			var fastWorkSheet = FastNpoiUtils.GetGenericInstance(typeof(FastSheet<>), new Type[1] { type }, new object[2] { this, sheet });
			return new FastDynamicSheet(fastWorkSheet, type);
		}

		public IFastDynamicSheet GetSheet(Type type, string name)
		{
			ValidateType(type);
			ISheet sheet = _workbook.GetSheet(name);
			var fastWorkSheet = FastNpoiUtils.GetGenericInstance(typeof(FastSheet<>), new Type[1] { type }, new object[2] { this, sheet });
			return new FastDynamicSheet(fastWorkSheet, type);
		}

        private void ValidateType(Type type)
        {
            if (!type.IsClass)
            {
                throw new ArgumentException("type must be class!!");
            }
			if(type.GetConstructors().Where(t => t.GetParameters().Length == 0) == null)
            {
				throw new ArgumentException("type must have an non-parameter constructor!!");
			}
		}
	}
}

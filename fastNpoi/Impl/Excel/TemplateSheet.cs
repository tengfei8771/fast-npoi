﻿using fastNpoi.Interface;
using fastNpoi.Interface.Excel;
using fastNpoi.Utils;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace fastNpoi.Impl.Excel
{
    public class TemplateSheet : ITemplateSheet
    {
        private IFastWorkbook fastWorkbook;
        private ISheet sheet;
        private Dictionary<string,object> dataMap = new Dictionary<string,object>();
        private IDataOperation dataOperation = new DefaultOperation();
        private Dictionary<string,(Type type,IEnumerable<object> objects)> dataListMap=new Dictionary<string, (Type,IEnumerable<object>)>();
        private Dictionary<int, (Dictionary<int, (string paramName, PropertyInfo prop)> columnInfo, HashSet<(string paramName, int listCount)> columnParams)> dataPointMap = new Dictionary<int, (Dictionary<int, (string paramName, PropertyInfo prop)> columnInfo, HashSet<(string paramName, int listCount)> columnParams)>();

        public TemplateSheet(IFastWorkbook fastWorkbook,ISheet sheet)
        {
            this.sheet = sheet;
            this.fastWorkbook = fastWorkbook;
        }
        public ITemplateSheet SetData<T>(string paramName, T data) where T : class, new()
        {
            dataMap.Add(paramName, data);
            return this;
        }

        public ITemplateSheet SetDataList<T>(string paramName, IEnumerable<T> dataList) where T : class, new()
        {
            dataListMap.Add(paramName,(typeof(T),dataList));
            return this;
        }

        public IFastWorkbook WriteToSheet()
        {
            WriteDataToSheet();
            WriteListToSheet();
            return fastWorkbook;
        }
        private void WriteDataToSheet()
        {
            int rowNum = sheet.LastRowNum+1;
            for (int i = 0; i < rowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null)
                {
                    continue;
                }
                Dictionary<int, (string paramName, PropertyInfo prop)> columnInfo = new Dictionary<int, (string paramName, PropertyInfo prop)>();
                HashSet<(string, int)> columnParams = new HashSet<(string, int)>();
                int columnNum = row.LastCellNum;
                string arrExpression = string.Empty;
                bool isStart = false;
                for (int j = 0; j < columnNum; j++)
                {
                    ICell cell = row.GetCell(j);
                    if (cell == null)
                    {
                        continue;
                    }
                    if (cell.CellType != CellType.String)
                    {
                        continue;
                    }
                    string cellStringValue = cell.StringCellValue;
                    string content = FastNpoiUtils.GetRegValue(cellStringValue, "{", "}");
                    if (content != "")
                    {
                        if (content.Contains("."))
                        {
                            var dataExpress = content.Split('.');
                            if (dataMap.TryGetValue(dataExpress[0], out var data))
                            {
                                var prop = data.GetType().GetProperty(dataExpress[1]);
                                if (prop != null)
                                {
                                    var dataValue = dataOperation.GetValue(data, prop);
                                    FastNpoiUtils.SetCellValue(cell, dataValue);
                                }

                            }
                        }
                    }
                    if (cellStringValue.StartsWith("{{") || cellStringValue.StartsWith("{{{"))
                    {
                        isStart = true;
                    }
                    if (isStart)
                    {
                        arrExpression += $"{cellStringValue},";
                        if (cellStringValue.Contains("."))
                        {
                            string[] infos = cellStringValue.Replace("{{", "").Replace("}}", "").Split('.');
                            string paramName = infos[0];
                            string propertyName = infos[1];
                            if (dataListMap.TryGetValue(paramName, out var dataList))
                            {
                                PropertyInfo prop = dataList.type.GetProperty(propertyName);
                                if (prop != null)
                                {
                                    columnInfo.Add(j, (paramName, prop));
                                    columnParams.Add((paramName, dataList.objects.Count()));
                                }
                            }
                        }

                    }
                    if (cellStringValue.EndsWith("}}")|| cellStringValue.EndsWith("}}}"))
                    {
                        isStart = false;
                    }
                }
                if (!string.IsNullOrWhiteSpace(arrExpression) && FastNpoiUtils.GetRegValue(arrExpression.TrimEnd(','), "{{", "}}") != "")
                {
                    dataPointMap.Add(i, (columnInfo, columnParams));
                }
            }
        }

        private void WriteListToSheet()
        {
            int offset = 0;
            foreach(var rowinfo in dataPointMap)
            {
                int maxCount = rowinfo.Value.columnParams.Max(t => t.listCount)-1;
                IRow sourceRow = sheet.GetRow(rowinfo.Key+offset);
                CopyRows(sourceRow, rowinfo.Key+offset, sheet.LastRowNum, maxCount);
                foreach (var column in rowinfo.Value.columnParams)
                {
                    var list = dataListMap[column.paramName].objects.ToList();
                    for(int i=0;i <= maxCount; i++)
                    {
                        IRow row = sheet.GetRow(rowinfo.Key + offset + i);
                        foreach (var columnRecord in rowinfo.Value.columnInfo)
                        {
                            if (columnRecord.Value.paramName != column.paramName)
                            {
                                continue;
                            }
                            ICell cell = row.GetCell(columnRecord.Key);
                            if (cell == null)
                            {
                                continue;
                            }
                            if (i <= list.Count - 1)
                            {
                                var val = dataOperation.GetValue(list[i], columnRecord.Value.prop);
                                FastNpoiUtils.SetCellValue(cell, val);
                            }
                            else
                            {
                                FastNpoiUtils.SetCellValue(cell, "");
                            }
                            
                        }
                    }
                }
                offset += maxCount;
            }
        }

        private void CopyRows(IRow sourceRow,int regionStartIndex,int regionEndIndex,int offsetRows)
        {
            if(regionStartIndex> regionEndIndex)
            {
                throw new ArgumentException("参数异常,起始索引数必须大于结束索引数！");
            }
            sheet.ShiftRows(regionStartIndex, regionEndIndex, offsetRows);
            for(int i= regionStartIndex;i< regionStartIndex + offsetRows; i++)
            {
                IRow shiftRow= sheet.GetRow(i);
                if (shiftRow != null)
                {
                    continue;
                }
                shiftRow = sheet.CreateRow(i);
                for (int j = 0; j < sourceRow.LastCellNum; j++)
                {
                    ICell sourceCell = sourceRow.GetCell(j);
                    ICell shiftCell = shiftRow.CreateCell(j);
                    if (sourceCell == null)
                    {
                        continue;
                    }
                    else
                    {
                        //复制样式
                        var style = fastWorkbook.Workbook.CreateCellStyle();
                        style.CloneStyleFrom(sourceCell.CellStyle);
                        shiftCell.CellStyle = style;
                        //复制批注
                        if (sourceCell.CellComment != null)
                        {
                            shiftCell.CellComment = sourceCell.CellComment;
                        }
                        //超链接
                        if (sourceCell.Hyperlink != null)
                        {
                            shiftCell.Hyperlink = sourceCell.Hyperlink;
                        }
                        //单元格类型
                        shiftCell.SetCellType(sourceCell.CellType);
                        //赋值
                        switch (sourceCell.CellType)
                        {
                            case CellType.Blank:
                                shiftCell.SetCellValue(sourceCell.StringCellValue);
                                break;
                            case CellType.Boolean:
                                shiftCell.SetCellValue(sourceCell.BooleanCellValue);
                                break;
                            case CellType.Error:
                                shiftCell.SetCellErrorValue(sourceCell.ErrorCellValue);
                                break;
                            case CellType.Formula:
                                shiftCell.SetCellFormula(sourceCell.CellFormula);
                                break;
                            case CellType.Numeric:
                                shiftCell.SetCellValue(sourceCell.NumericCellValue);
                                break;
                            case CellType.String:
                                shiftCell.SetCellValue(sourceCell.RichStringCellValue);
                                break;
                            case CellType.Unknown:
                                shiftCell.SetCellValue(sourceCell.StringCellValue);
                                break;
                        }

                    }
                }
                for (int k = 0; k < sheet.NumMergedRegions; k++)
                {
                    CellRangeAddress cellRangeAddress = sheet.GetMergedRegion(k);
                    if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                    {
                        CellRangeAddress newCellRangeAddress = new CellRangeAddress(shiftRow.RowNum,
                                                                                    (shiftRow.RowNum +
                                                                                     (cellRangeAddress.FirstRow -
                                                                                      cellRangeAddress.LastRow)),
                                                                                    cellRangeAddress.FirstColumn,
                                                                                    cellRangeAddress.LastColumn);
                        sheet.AddMergedRegion(newCellRangeAddress);
                    }
                }
            }
        }

        private void CopyRow(int sorceRowNum,int targetRowNum)
        {
            IRow sourceRow = sheet.GetRow(sorceRowNum);
            IRow targetRow = sheet.GetRow(targetRowNum);
            int rowNum = sheet.LastRowNum;
            if (targetRow != null)
            {
                sheet.ShiftRows(targetRowNum, rowNum,1);
            }
            else
            {
                targetRow = sheet.CreateRow(targetRowNum);
            }
            for(int i = 0; i < sourceRow.LastCellNum; i++)
            {
                ICell sourceCell= sourceRow.GetCell(i);
                ICell targetCell=targetRow.CreateCell(i);
                if (sourceCell == null)
                {
                    continue;
                }
                else
                {
                    //复制样式
                    var style = fastWorkbook.Workbook.CreateCellStyle();
                    style.CloneStyleFrom(sourceCell.CellStyle);
                    targetCell.CellStyle=style;
                    //复制批注
                    if (sourceCell.CellComment != null)
                    {
                        targetCell.CellComment = sourceCell.CellComment;
                    }
                    //超链接
                    if (sourceCell.Hyperlink != null)
                    {
                        targetCell.Hyperlink = sourceCell.Hyperlink;
                    }
                    //单元格类型
                    targetCell.SetCellType(sourceCell.CellType);
                    //赋值
                    switch (sourceCell.CellType)
                    {
                        case CellType.Blank:
                            targetCell.SetCellValue(sourceCell.StringCellValue);
                            break;
                        case CellType.Boolean:
                            targetCell.SetCellValue(sourceCell.BooleanCellValue);
                            break;
                        case CellType.Error:
                            targetCell.SetCellErrorValue(sourceCell.ErrorCellValue);
                            break;
                        case CellType.Formula:
                            targetCell.SetCellFormula(sourceCell.CellFormula);
                            break;
                        case CellType.Numeric:
                            targetCell.SetCellValue(sourceCell.NumericCellValue);
                            break;
                        case CellType.String:
                            targetCell.SetCellValue(sourceCell.RichStringCellValue);
                            break;
                        case CellType.Unknown:
                            targetCell.SetCellValue(sourceCell.StringCellValue);
                            break;
                    }

                }
            }
            for (int i = 0; i < sheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = sheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(targetRow.RowNum,
                                                                                (targetRow.RowNum +
                                                                                 (cellRangeAddress.FirstRow -
                                                                                  cellRangeAddress.LastRow)),
                                                                                cellRangeAddress.FirstColumn,
                                                                                cellRangeAddress.LastColumn);
                    sheet.AddMergedRegion(newCellRangeAddress);
                }
            }

        }

    }
}

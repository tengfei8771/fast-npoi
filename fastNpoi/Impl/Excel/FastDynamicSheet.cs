﻿using fastNpoi.Interface;
using fastNpoi.Interface.Excel;
using fastNpoi.Model;
using fastNpoi.Model.Enum;
using fastNpoi.Utils;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace fastNpoi.Impl.Excel
{
	public class FastDynamicSheet : IFastDynamicSheet
	{
		private dynamic fastSheet;
		private Type type;
		public FastDynamicSheet(dynamic fastSheet, Type type)
		{
			this.fastSheet = fastSheet;
			this.type = type;
		}
		public IFastDynamicSheet CommentCell(List<CommentModel> commentModels)
		{
			fastSheet.CommentCell(commentModels);
			return this;
		}

		public IFastDynamicSheet CommentCell(Func<Dictionary<string, int>, List<CommentModel>> func)
		{
			fastSheet.CommentsCell(func);
			return this;
		}

		public IFastDynamicSheet DataMapper(Action<dynamic> action)
		{
			fastSheet.DataMapper(action);
			return this;
		}

		public IFastDynamicSheet DisableOrderby()
		{
			fastSheet.DisableOrderby();
			return this;
		}

		

		public IFastDynamicSheet HeaderMapper(string headerName, string propertyName, int width = 0, OperationType operationType = OperationType.Import_Export, int position = 999, bool mergeCell = false, DataType dataType = DataType.Text)
		{
			fastSheet.HeaderMapper(headerName, propertyName, width, operationType, position, mergeCell, dataType);
			return this;
		}

		public IFastDynamicSheet MergeCell(int startRowIndex, int endRowIndex, int startColumnIndex, int endColumnIndex, int sheetIndex = -1)
		{
			fastSheet.MergeCell(startRowIndex, endRowIndex, startColumnIndex, endColumnIndex, sheetIndex);
			return this;
		}

		public IFastDynamicSheet SetColumnSkipNumber(int offset)
		{
			fastSheet.SetColumnSkipNumber(offset);
			return this;
		}

		public IFastDynamicSheet SetColumnStartIndex(int columnStartIndex)
		{
			fastSheet.SetColumnStartIndex(columnStartIndex);
			return this;
		}

		public IFastDynamicSheet SetCommentCellStyle(Func<IWorkbook, ICellStyle> styleFunc)
		{
			fastSheet.SetCommentCellStyle(styleFunc);
			return this;
		}

		public IFastDynamicSheet SetCommentVisable(bool visible)
		{
			fastSheet.SetCommentVisable(visible); 
			return this;
		}

		public IFastDynamicSheet SetDataHeight(short height)
		{
			fastSheet.SetDataHeight(height);
			return this;
		}

		public IFastDynamicSheet SetDataList(dynamic list)
		{
			if (!list.GetType().IsGenericType||(list.GetType().GetGenericTypeDefinition()!=typeof(List<>)))
			{
				throw new ArgumentException($"list must be List<T>!!!");
			}
			var list1 = Enumerable.ToList(Enumerable.Cast<object>(list));
			var type = Enumerable.First(list1).GetType();
			if (this.type != type)
			{
				throw new ArgumentException($"List<T> must be List<{this.type.Name}>!!");
			}
			fastSheet.SetDataList(list);
			return this;
		}

		public IFastDynamicSheet SetDataOperation(IDataOperation operation)
		{
			fastSheet.setDataOperation(operation);
			return this;
		}

		public IFastDynamicSheet SetDataStyle(Func<IWorkbook, ICellStyle> styleFunc)
		{
			fastSheet.SetDataStyle(styleFunc);
			return this;
		}

		public IFastDynamicSheet SetHeaderHeight(short height)
		{
			fastSheet.SetHeaderHeight(height);
			return this;
		}

		public IFastDynamicSheet SetHeaderIndex(int headerIndex)
		{
			fastSheet.SetHeaderIndex(headerIndex);
			return this;
		}

		public IFastDynamicSheet SetHeaderSkipNumber(int offset)
		{
			fastSheet.SetHeaderSkipNumber(offset);
			return this;
		}

		public IFastDynamicSheet SetHeaderStyle(Func<IWorkbook, ICellStyle> styleFunc)
		{
			fastSheet.SetHeaderStyle(styleFunc);
			return this;
		}

		public IFastDynamicSheet SetIgnoreProperty(Expression<Func<T, object>> propertyExpression)
		{
			fastSheet.SetIgnoreProperty(propertyExpression);
			return this;
		}

		public IFastDynamicSheet SetIgnoreProperty(string propertyName)
		{
			fastSheet.SetIgnoreProperty(propertyName);
			return this;
		}

		public IFastDynamicSheet SetJsonMapping(Expression<Func<T, object>> jsonPropertyExpression, Dictionary<string, string> map)
		{
			fastSheet.SetJsonMapping(this, jsonPropertyExpression, map);
			return this;
		}

		public IFastDynamicSheet SetJsonMapping(string jsonPropertyName, Dictionary<string, string> map)
		{
			fastSheet.SetJsonMapping(jsonPropertyName, map);
			return this;
		}

		public IFastDynamicSheet SetPageLimit(int pageLimt)
		{
			fastSheet.SetPageLimit(pageLimt);
			return this;
		}

		public IFastDynamicSheet SetRowSkipNumber(int offset)
		{
			fastSheet.SetRowSkipNumber(offset);
			return this;
		}

		public IFastDynamicSheet SheetMapper(Action<ISheet> action)
		{
			fastSheet.SheetMapper(action);
			return this;
		}

		public dynamic ToList()
		{
			return fastSheet.ToList();
		}

		public IFastWorkbook WriteToSheet()
		{
			return fastSheet.WriteToSheet();
		}
	}
}

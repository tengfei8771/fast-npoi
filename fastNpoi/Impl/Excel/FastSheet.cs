﻿using fastNpoi.Attributes;
using fastNpoi.Interface;
using fastNpoi.Interface.Excel;
using fastNpoi.Model;
using fastNpoi.Model.Enum;
using fastNpoi.Utils;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace fastNpoi.Impl.Excel
{
    public class FastSheet<T> : IFastSheet<T> where T : class, new()
    {
        public IFastWorkbook FastWorkbook { get; }
        public ISheet Sheet { get; private set; } = null;
        /// <summary>
        /// 表头坐标行
        /// </summary>
        public int HeaderIndex { get; private set; } = 0;
        /// <summary>
        /// 起始列坐标列
        /// </summary>
        public int ColumnStartIndex { get; private set; } = 0;
        /// <summary>
        /// 每一行数据间隔行数
        /// </summary>
        public int EveryRowSkipNumber { get; private set; } = 1;
        /// <summary>
        /// 每一列间隔列数
        /// </summary>
        public int EveryColumnSkipNumber { get; private set; } = 1;
        /// <summary>
        /// 表头和数据之间的间隔
        /// </summary>
        public int HeaderSkipNumber { get; private set; } = 1;
        /// <summary>
        /// 每个sheet页的数据条数
        /// </summary>
        public int PageLimit { get; private set; } = 0;
        /// <summary>
        /// 是否排序
        /// </summary>
        public bool IsOrderby { get; private set; } = true;
        /// <summary>
        /// 数据取值赋值实例
        /// </summary>
        public IDataOperation DataOperation { get; private set; } = new DefaultOperation();
        /// <summary>
        /// 数据操作委托list
        /// </summary>
        public List<Action<T>> ActionList { get; private set; } = new List<Action<T>>();
        /// <summary>
        /// 实体字段的特性数据记录
        /// </summary>
        public Dictionary<PropertyInfo, NpoiAttribute> ExtraInfo { get; private set; } = new Dictionary<PropertyInfo, NpoiAttribute>();
        /// <summary>
        /// 合并表格记录字段
        /// </summary>
        public Dictionary<int, List<CellRangeAddress>> MergeRecord { get; private set; } = new Dictionary<int, List<CellRangeAddress>>()
        {
            {-1,new List<CellRangeAddress>() }
        };
        /// <summary>
        /// 批注list
        /// </summary>
        public List<CommentModel> CommentList { get; private set; } = new List<CommentModel>();
        /// <summary>
        /// sheet页委托list
        /// </summary>
        public List<Action<ISheet>> SheetActionList { get; private set; } = new List<Action<ISheet>>();
        /// <summary>
        /// 表头样式
        /// </summary>
        public ICellStyle HeaderStyle { get; private set; } = null;
        /// <summary>
        /// 数据样式
        /// </summary>
        public ICellStyle DataStyle { get; private set; } = null;
        /// <summary>
        /// 批注样式
        /// </summary>
        public ICellStyle CommentCellStyle { get; private set; } = null;
        /// <summary>
        /// 数据
        /// </summary>
        public List<T> DataList { get; private set; } = new List<T>();
        /// <summary>
        /// 批注默认可见性
        /// </summary>
        public bool CommentVisible { get; private set; } = false;
        /// <summary>
        /// json映射
        /// </summary>
        public Dictionary<PropertyInfo, Dictionary<string, string>> JsonMapping { get; private set; } = new Dictionary<PropertyInfo, Dictionary<string, string>>();
        /// <summary>
        /// 表头行高度
        /// </summary>
        public short HeaderHeight { get; private set; }
        /// <summary>
        /// 数据行高度
        /// </summary>
        public short DataHeight { get; private set; }
        /// <summary>
        /// 忽略的属性list
        /// </summary>
        private List<PropertyInfo> IgnorePropertyinfos = new List<PropertyInfo>();
        /// <summary>
        /// 字段映射(导出)规则
        /// </summary>
        private Dictionary<string, Dictionary<object, object>> Prop2Cell = new Dictionary<string, Dictionary<object, object>>();
        /// <summary>
        /// 字段映射(导入)规则
        /// </summary>
        private Dictionary<string, Dictionary<string, object>> Cell2Prop = new Dictionary<string, Dictionary<string, object>>();
        /// <summary>
        /// 自适应宽度标志
        /// </summary>
        public bool AutoSize { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fastWorkboox"></param>
        /// <param name="sheet"></param>
        public FastSheet(IFastWorkbook fastWorkboox, ISheet sheet)
        {
            FastWorkbook = fastWorkboox;
            Sheet = sheet;
        }

        public IFastSheet<T> CommentCell(List<CommentModel> commentModels)
        {
            CommentList.AddRange(commentModels);
            return this;
        }

        public IFastSheet<T> CommentCell(Func<Dictionary<string, int>, List<CommentModel>> func)
        {
            var header = GetHeader(Sheet);
            CommentList.AddRange(func.Invoke(header));
            return this;
        }



        public IFastSheet<T> DataMapper(Action<T> action)
        {
            ActionList.Add(action);
            return this;
        }

        public IFastSheet<T> DisableOrderby()
        {
            IsOrderby = true;
            return this;
        }

        public IFastSheet<T> HeaderMapper(string headerName, Expression<Func<T, object>> propertyExpression, int width = 0, OperationType operationType = OperationType.Import_Export, int position = 999, bool mergeCell = false, DataType dataType = DataType.Text)
        {
            string propertyName = FastNpoiUtils.GetExpPropertyName(propertyExpression.Body)?.ToString();
            PropertyInfo property = typeof(T).GetProperty(propertyName);
            NpoiAttribute attribute = new NpoiAttribute(headerName, width, position, operationType, mergeCell, dataType);
            ExtraInfo.Add(property, attribute);
            return this;
        }

        public IFastSheet<T> HeaderMapper(string headerName, string propertyName, int width = 0, OperationType operationType = OperationType.Import_Export, int position = 999, bool mergeCell = false, DataType dataType = DataType.Text)
        {
            PropertyInfo property = typeof(T).GetProperty(headerName);
            NpoiAttribute attribute = new NpoiAttribute(headerName, width, position, operationType, mergeCell, dataType);
            ExtraInfo.Add(property, attribute);
            return this;
        }

        public IFastSheet<T> MergeCell(int startRowIndex, int endRowIndex, int startColumnIndex, int endColumnIndex, int sheetIndex = -1)
        {
            CellRangeAddress cellRangeAddress = new CellRangeAddress(startRowIndex, endRowIndex, startColumnIndex, endColumnIndex);
            var sheetRecord = MergeRecord[sheetIndex];
            sheetRecord.Add(cellRangeAddress);
            return this;
        }

        public IFastSheet<T> SetColumnSkipNumber(int offset)
        {
            EveryColumnSkipNumber = offset;
            return this;
        }

        public IFastSheet<T> SetColumnStartIndex(int columnStartIndex)
        {
            ColumnStartIndex = columnStartIndex;
            return this;
        }

        public IFastSheet<T> SetCommentCellStyle(Func<IWorkbook, ICellStyle> styleFunc)
        {
            CommentCellStyle = styleFunc.Invoke(FastWorkbook.Workbook);
            return this;
        }

        public IFastSheet<T> SetCommentVisable(bool visible)
        {
            CommentVisible = true;
            return this;
        }

        public IFastSheet<T> SetDataList(List<T> list)
        {
            DataList = list;
            return this;
        }

        public IFastSheet<T> SetDataOperation(IDataOperation operation)
        {
            DataOperation = operation;
            return this;
        }

        public IFastSheet<T> SetDataStyle(Func<IWorkbook, ICellStyle> styleFunc)
        {
            DataStyle = styleFunc.Invoke(FastWorkbook.Workbook);
            return this;
        }

        public IFastSheet<T> SetHeaderIndex(int headerIndex)
        {
            HeaderIndex = headerIndex;
            return this;
        }

        public IFastSheet<T> SetHeaderSkipNumber(int offset)
        {
            HeaderSkipNumber = offset;
            return this;
        }

        public IFastSheet<T> SetHeaderStyle(Func<IWorkbook, ICellStyle> styleFunc)
        {
            HeaderStyle = styleFunc.Invoke(FastWorkbook.Workbook);
            return this;
        }

        public IFastSheet<T> SetJsonMapping(Expression<Func<T, object>> jsonPropertyExpression, Dictionary<string, string> map)
        {
            string propertyName = FastNpoiUtils.GetExpPropertyName(jsonPropertyExpression.Body)?.ToString();
            PropertyInfo property = typeof(T).GetProperty(propertyName);
            JsonMapping.Add(property, map);
            return this;
        }

        public IFastSheet<T> SetJsonMapping(string jsonPropertyName, Dictionary<string, string> map)
        {
            PropertyInfo property = typeof(T).GetProperty(jsonPropertyName);
            JsonMapping.Add(property, map);
            return this;
        }

        public IFastSheet<T> SetPageLimit(int pageLimt)
        {
            PageLimit = pageLimt;
            return this;
        }

        public IFastSheet<T> SetRowSkipNumber(int offset)
        {
            EveryRowSkipNumber = offset;
            return this;
        }

        public IFastSheet<T> SheetMapper(Action<ISheet> action)
        {
            SheetActionList.Add(action);
            return this;
        }


        //public IFastWorkbook WriteToSheet()
        //{
        //    ExecuteSheetAction();
        //    ExecuteComment();
        //    return FastWorkbook;
        //}
        public IFastWorkbook WriteToSheet()
        {
            Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> dic = FastNpoiUtils.GetHeaderAttrDic<T>(OperationType.Export, IsOrderby, ExtraInfo, IgnorePropertyinfos);
            if (PageLimit == 0)
            {
                MergeRecord.Add(0, new List<CellRangeAddress>());
                WriteStaticHeader(dic, this.Sheet);
                var header = GetHeader(Sheet);
                WriteDynamicHeader(Sheet, header);
                WriteData(Sheet, DataList, 1, dic, header);
                AutoSizeColumn(Sheet);
            }
            else
            {
                int page = (DataList.Count() + PageLimit - 1) / PageLimit;
                for (int p = 1; p <= page; p++)
                {
                    ISheet sheet;
                    MergeRecord.Add(p - 1, new List<CellRangeAddress>());
                    if (p == 1)
                    {
                        sheet = Sheet;
                    }
                    else
                    {
                        sheet = FastWorkbook.Workbook.CreateSheet();
                    }
                    WriteStaticHeader(dic, sheet);
                    var header = GetHeader(sheet);
                    WriteDynamicHeader(sheet, header);
                    WriteData(sheet, DataList.Skip((p - 1) * PageLimit).Take(PageLimit).ToList(), p, dic, header);
                    AutoSizeColumn(sheet);
                }

            }
            ExecuteComment();
            return FastWorkbook;
        }

        /// <summary>
        /// 静态表头写入
        /// </summary>
        /// <param name="headerDic"></param>
        /// <param name="sheet"></param>
        private void WriteStaticHeader(Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> headerDic, ISheet sheet)
        {
            IRow headerRow = sheet.CreateRow(HeaderIndex);
            if (HeaderHeight != 0)
            {
                headerRow.HeightInPoints = HeaderHeight;
            }
            var dataHeaderList = headerDic.Keys.ToArray();
            //静态表头赋值
            for (int i = 0; i < headerDic.Keys.Count; i++)
            {
                int cellIndex = ColumnStartIndex + i * EveryColumnSkipNumber;
                ICell headerCell = headerRow.CreateCell(cellIndex);
                var attr = headerDic[dataHeaderList[i]].attr;
                if (attr.ColumnWidth != 0)
                {
                    sheet.SetColumnWidth(cellIndex, attr.ColumnWidth);
                }
                headerCell.SetCellValue(dataHeaderList[i]);
                if (HeaderStyle != null)
                {
                    headerCell.CellStyle = HeaderStyle;
                }
            }
        }
        /// <summary>
        /// 写入data
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dataList"></param>
        /// <param name="page"></param>
        /// <param name="headerDic"></param>
        /// <param name="header"></param>
        private void WriteData(ISheet sheet, List<T> dataList, int page,
            Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> headerDic, Dictionary<string, int> header)
        {
            Dictionary<int, int> mergeCache = new Dictionary<int, int>();
            for (int i = 0; i < dataList.Count(); i++)
            {
                var data = dataList[i];
                foreach (var action in ActionList)
                {
                    action.Invoke(data);
                }
                if (data == null)
                {
                    continue;
                }
                int rowIndex = HeaderIndex + HeaderSkipNumber + i * EveryRowSkipNumber;
                IRow row = sheet.CreateRow(rowIndex);
                if (DataHeight != 0)
                {
                    row.HeightInPoints = DataHeight;
                }
                var headerKeys = header.Keys.ToList();
                //静态数据赋值
                for (int j = 0; j < headerKeys.Count(); j++)
                {
                    int cellIndex = header[headerKeys[j]];
                    ICell cell = row.CreateCell(cellIndex);
                    var dicValue = headerDic[headerKeys[j]];
                    object value = DataOperation.GetValue(data, dicValue.prop);
                    if (value == null)
                    {
                        continue;
                    }
                    if (dicValue.attr.DataType == DataType.Image)
                    {
                        FastNpoiUtils.CreatePictureCell(FastWorkbook.Workbook, sheet, value.ToString(), HeaderIndex + HeaderSkipNumber + i, j);
                    }
                    else
                    {
                        //这里处理值的转换
                        if (Prop2Cell.TryGetValue(dicValue.prop.Name, out Dictionary<object, object> dic))
                        {
                            if (dic.TryGetValue(value, out object mapValue))
                            {
                                value = mapValue;
                            }
                        }
                        FastNpoiUtils.SetCellValue(cell, dicValue.attr.DataType, value);
                    }
                    //cell.SetCellValue(value?.ToString());
                    if (DataStyle != null)
                    {
                        cell.CellStyle = DataStyle;
                    }
                    if (dicValue.attr.MergeCell)
                    {
                        int lastRowIndex = HeaderIndex + HeaderSkipNumber + (i - 1) * EveryRowSkipNumber;
                        //第一条数据不找上一条,直接加入
                        if (i == 0)
                        {
                            mergeCache.Add(cellIndex, HeaderIndex + HeaderSkipNumber);
                        }
                        else
                        {
                            object preValue = DataOperation.GetValue(dataList[i - 1], dicValue.prop);
                            if (!Equals(preValue, value))
                            {
                                if (mergeCache[cellIndex] != lastRowIndex)
                                {
                                    MergeCell(mergeCache[cellIndex], lastRowIndex, cellIndex, cellIndex, page - 1);
                                }

                                mergeCache[cellIndex] = rowIndex;
                            }
                            else
                            {
                                //如果最后一行也需要合并 特殊处理
                                if (i == dataList.Count() - 1)
                                {
                                    MergeCell(mergeCache[cellIndex], rowIndex, cellIndex, cellIndex, page - 1);
                                }
                            }

                        }
                    }

                }
                WriteJsonData(DataList[i], row, header.Values.Max());
            }
            foreach (var sheetAction in SheetActionList)
            {
                sheetAction.Invoke(sheet);
            }
            //执行对所有sheet都生效的合并操作
            foreach (var range in MergeRecord[-1])
            {
                sheet.AddMergedRegion(range);
            }
            //执行对专属sheet生效的合并操作
            foreach (var range in MergeRecord[page - 1])
            {
                sheet.AddMergedRegion(range);
            }
        }
        /// <summary>
        /// 自使用宽度，不使用自带的AutoSizeColumn
        /// </summary>
        /// <param name="sheet"></param>
        private void AutoSizeColumn(ISheet sheet)
        {
            if (AutoSize)
            {
                Dictionary<int, int> sizeDic = new Dictionary<int, int>();
                for (int rowNum = 0; rowNum < sheet.LastRowNum; rowNum++)
                {
                    IRow currentRow;
                    //当前行未被使用过
                    if (sheet.GetRow(rowNum) == null)
                    {
                        continue;
                    }
                    else
                    {
                        currentRow = sheet.GetRow(rowNum);
                    }
                    for (int columnNum = 0; columnNum < currentRow.LastCellNum; columnNum++)
                    {
                        ICell currentCell = currentRow.GetCell(columnNum);
                        if (currentCell == null)
                        {
                            continue;
                        }
                        else
                        {
                            int length = Encoding.Default.GetBytes(currentCell.ToString()).Length;
                            if(sizeDic.TryGetValue(columnNum,out int size))
                            {
                                if (size < length)
                                {
                                    sizeDic[columnNum] = length + 1;
                                }
                            }
                            else
                            {
                                sizeDic.Add(columnNum, length+1);
                            }
                        }
                    }
                }
                foreach(var keyValue in sizeDic)
                {
                    sheet.SetColumnWidth(keyValue.Key, keyValue.Value*256);
                }
            }
            else
            {
                return;
            }
        }

        private void WriteJsonData(T data, IRow row, int staticColumnIndex)
        {
            var jsonPropertys = JsonMapping.Keys.ToArray();
            for (int z = 0; z < jsonPropertys.Count(); z++)
            {
                var propKey = jsonPropertys[z];
                var map = JsonMapping[propKey];
                var mapkeys = map.Keys.ToArray();
                try
                {
                    object jsonValue = DataOperation.GetValue(data, propKey);
                    if (jsonValue == null || string.IsNullOrWhiteSpace(jsonValue.ToString()))
                    {
                        break;
                    }
                    JObject obj = JObject.Parse(jsonValue.ToString());
                    for (int m = 0; m < map.Count; m++)
                    {
                        var dynamicKey = map[mapkeys[m]].ToString();
                        if (obj.TryGetValue(dynamicKey, out JToken dynamicValue))
                        {
                            string dynamicValueStr = dynamicValue.ToString();
                            int cellIndex = staticColumnIndex + (z + 1) * (m + 1) * EveryColumnSkipNumber;
                            var cell = row.CreateCell(cellIndex);
                            cell.SetCellValue(dynamicValueStr);
                        }
                    }
                }
                catch
                {
                    throw new Exception("Json转换失败!请确认字段是否正确！");
                }
            }
        }
        /// <summary>
        /// 动态表头写入 即json字段
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="header">表头字典</param>
        private void WriteDynamicHeader(ISheet sheet, Dictionary<string, int> header)
        {
            //Json表头写入
            var jsonPropertys = JsonMapping.Keys.ToArray();
            var staticColumnIndex = header.Values.Max();
            for (int i = 0; i < JsonMapping.Count; i++)
            {
                var headerRow = sheet.GetRow(HeaderIndex);
                var dynamicHeaderKeys = JsonMapping[jsonPropertys[i]].Keys.ToArray();
                for (int j = 0; j < dynamicHeaderKeys.Count(); j++)
                {
                    int cellIndex = staticColumnIndex + ColumnStartIndex + (i + 1) * (j + 1) * EveryColumnSkipNumber;
                    ICell headerCell = headerRow.CreateCell(cellIndex);
                    headerCell.SetCellValue(dynamicHeaderKeys[j]);
                }
            }
        }
        private void ExecuteSheetAction()
        {
            if (PageLimit < 0 || PageLimit >= 65535 - HeaderIndex)
            {
                throw new Exception("每页条数不能小于0且不能大于65535减去表头的偏移量！");
            }
            Dictionary<string, (PropertyInfo prop, NpoiAttribute attr)> dic = FastNpoiUtils.GetHeaderAttrDic<T>(OperationType.Export, IsOrderby, ExtraInfo, IgnorePropertyinfos);
            int page;
            if (PageLimit == 0)
            {
                page = 1;
            }
            else
            {
                page = (DataList.Count() + PageLimit - 1) / PageLimit;
            }
            for (int p = 1; p <= page; p++)
            {
                ISheet sheet;
                Dictionary<int, int> mergeCache = new Dictionary<int, int>();
                MergeRecord.Add(p - 1, new List<CellRangeAddress>());
                if (p == 1)
                {
                    sheet = Sheet;
                }
                else
                {
                    sheet = FastWorkbook.Workbook.CreateSheet();
                }
                IRow headerRow = sheet.CreateRow(HeaderIndex);
                if (HeaderHeight != 0)
                {
                    headerRow.HeightInPoints = HeaderHeight;
                }
                var DataHeaderList = dic.Keys.ToArray();
                //静态表头赋值
                for (int i = 0; i < dic.Keys.Count; i++)
                {
                    int cellIndex = ColumnStartIndex + i * EveryColumnSkipNumber;
                    ICell headerCell = headerRow.CreateCell(cellIndex);
                    var attr = dic[DataHeaderList[i]].attr;
                    if (attr.ColumnWidth != 0)
                    {
                        sheet.SetColumnWidth(cellIndex, attr.ColumnWidth);
                    }
                    headerCell.SetCellValue(DataHeaderList[i]);
                    if (HeaderStyle != null)
                    {
                        headerCell.CellStyle = HeaderStyle;
                    }
                }
                //获取静态header的列头和位置
                var header = GetHeader(sheet);
                var ExcelHeaderList = header.Keys.ToArray();
                //Json表头写入
                var jsonPropertys = JsonMapping.Keys.ToArray();
                var staticColumnIndex = header.Values.Max();
                for (int i = 0; i < JsonMapping.Count; i++)
                {
                    headerRow = sheet.GetRow(HeaderIndex);
                    var dynamicHeaderKeys = JsonMapping[jsonPropertys[i]].Keys.ToArray();
                    for (int j = 0; j < dynamicHeaderKeys.Count(); j++)
                    {
                        int cellIndex = staticColumnIndex + ColumnStartIndex + (i + 1) * (j + 1) * EveryColumnSkipNumber;
                        ICell headerCell = headerRow.CreateCell(cellIndex);
                        headerCell.SetCellValue(dynamicHeaderKeys[j]);
                    }
                }
                //数据赋值
                for (int i = (p - 1) * PageLimit; i < (p * PageLimit > DataList.Count ? DataList.Count : p * PageLimit); i++)
                {
                    var data = DataList[i];
                    if (data == null)
                    {
                        continue;
                    }
                    foreach (var action in ActionList)
                    {
                        action.Invoke(data);
                    }
                    int rowIndex = HeaderIndex + HeaderSkipNumber + i * EveryRowSkipNumber - PageLimit * (p - 1);
                    IRow row = sheet.CreateRow(rowIndex);
                    if (DataHeight != 0)
                    {
                        row.HeightInPoints = DataHeight;
                    }
                    //静态数据赋值
                    for (int j = 0; j < ExcelHeaderList.Count(); j++)
                    {
                        int cellIndex = header[ExcelHeaderList[j]];
                        ICell cell = row.CreateCell(cellIndex);
                        var dicValue = dic[ExcelHeaderList[j]];
                        object value = DataOperation.GetValue(data, dicValue.prop);
                        if (value == null)
                        {
                            continue;
                        }
                        if (dicValue.attr.DataType == DataType.Image)
                        {
                            FastNpoiUtils.CreatePictureCell(FastWorkbook.Workbook, sheet, value.ToString(), HeaderIndex + HeaderSkipNumber + i, j);
                        }
                        else
                        {
                            FastNpoiUtils.SetCellValue(cell, dicValue.attr.DataType, value);
                        }
                        //cell.SetCellValue(value?.ToString());
                        if (DataStyle != null)
                        {
                            cell.CellStyle = DataStyle;
                        }
                        if (dicValue.attr.MergeCell)
                        {
                            int lastRowIndex = HeaderIndex + HeaderSkipNumber + (i - 1) * EveryRowSkipNumber - PageLimit * (p - 1);
                            if (i - PageLimit * (p - 1) == 0)
                            {
                                mergeCache.Add(cellIndex, HeaderIndex + HeaderSkipNumber);
                            }
                            else
                            {
                                object preValue = DataOperation.GetValue(DataList[i - 1], dicValue.prop);
                                if (!Equals(preValue, value))
                                {
                                    if (mergeCache[cellIndex] != lastRowIndex)
                                    {
                                        MergeCell(mergeCache[cellIndex], lastRowIndex, cellIndex, cellIndex, p - 1);
                                    }

                                    mergeCache[cellIndex] = rowIndex;
                                }
                                else
                                {
                                    //如果最后一行也需要合并 特殊处理
                                    if (i == p * PageLimit - 1)
                                    {
                                        MergeCell(mergeCache[cellIndex], rowIndex, cellIndex, cellIndex, p - 1);
                                    }
                                }

                            }
                        }
                    }
                    //动态数据赋值
                    for (int z = 0; z < jsonPropertys.Count(); z++)
                    {
                        var propKey = jsonPropertys[z];
                        var map = JsonMapping[propKey];
                        var mapkeys = map.Keys.ToArray();
                        try
                        {
                            object jsonValue = DataOperation.GetValue(data, propKey);
                            if (jsonValue == null || string.IsNullOrWhiteSpace(jsonValue.ToString()))
                            {
                                break;
                            }
                            JObject obj = JObject.Parse(jsonValue.ToString());
                            for (int m = 0; m < map.Count; m++)
                            {
                                var dynamicKey = map[mapkeys[m]].ToString();
                                if (obj.TryGetValue(dynamicKey, out JToken dynamicValue))
                                {
                                    string dynamicValueStr = dynamicValue.ToString();
                                    int cellIndex = staticColumnIndex + (z + 1) * (m + 1) * EveryColumnSkipNumber;
                                    var cell = row.CreateCell(cellIndex);
                                    cell.SetCellValue(dynamicValueStr);
                                }
                            }
                        }
                        catch
                        {
                            throw new Exception("Json转换失败!请确认字段是否正确！");
                        }
                    }
                }
                foreach (var sheetAction in SheetActionList)
                {
                    sheetAction.Invoke(sheet);
                }
                //执行对所有sheet都生效的合并操作
                foreach (var range in MergeRecord[-1])
                {
                    sheet.AddMergedRegion(range);
                }
                //执行对专属sheet生效的合并操作
                foreach (var range in MergeRecord[p - 1])
                {
                    sheet.AddMergedRegion(range);
                }
                ExecuteComment();
            }
        }
        private void ExecuteComment()
        {
            IDrawing drawing = Sheet.CreateDrawingPatriarch();
            foreach (var commentModel in CommentList)
            {
                IRow row = Sheet.GetRow(commentModel.RowIndex);
                if (row == null)
                {
                    row = Sheet.CreateRow(commentModel.RowIndex);
                }
                ICell cell = row.GetCell(commentModel.ColumnIndex);
                if (cell == null)
                {
                    cell = row.CreateCell(commentModel.ColumnIndex);
                }
                IComment comment = cell.CellComment;
                if (comment == null)
                {
                    IClientAnchor clientAnchor = drawing.CreateAnchor(0, 0, 0, 3, 3, 3, 5, 6);
                    comment = drawing.CreateCellComment(clientAnchor);
                    comment.Visible = CommentVisible;
                    comment.String = FastWorkbook.Workbook.GetCreationHelper().CreateRichTextString(commentModel.Message);
                    cell.CellComment = comment;
                }
                else
                {
                    comment.String = FastWorkbook.Workbook.GetCreationHelper().CreateRichTextString(commentModel.Message + ";\n" + comment.String.String);
                }
                if (CommentCellStyle != null)
                {
                    cell.CellStyle = CommentCellStyle;
                }
            }
        }
        private Dictionary<string, int> GetHeader(ISheet sheet)
        {
            Dictionary<string, int> header = new Dictionary<string, int>();
            IRow row = sheet.GetRow(HeaderIndex);
            int columnNum = row.LastCellNum;
            for (int i = 0; i < columnNum; i++)
            {
                ICell cell = row.GetCell(i);
                if (cell == null)
                {
                    continue;
                }
                header.Add(cell.StringCellValue, i);
            }
            return header;
        }

        public List<T> ToList()
        {
            var pictureAnchors = FastNpoiUtils.GetPictureAnchors(Sheet);
            Dictionary<string, (PropertyInfo, NpoiAttribute)> headerAttrDic = FastNpoiUtils.GetHeaderAttrDic<T>(OperationType.Import, IsOrderby, ExtraInfo, IgnorePropertyinfos);
            List<T> list = new List<T>();
            int num = HeaderIndex + HeaderSkipNumber;
            Dictionary<string, int> header = GetHeader(Sheet);
            int lastRowNum = Sheet.LastRowNum;
            for (int i = num; i < lastRowNum + num; i++)
            {
                IRow row = Sheet.GetRow(i);
                if (row == null)
                {
                    continue;
                }

                T val = new T();
                foreach (KeyValuePair<string, int> item in header)
                {
                    if (headerAttrDic.TryGetValue(item.Key, out var value))
                    {
                        try
                        {
                            if (value.Item2.DataType == DataType.Image)
                            {
                                var pictureAnchor = pictureAnchors.Where(t => t.Row1 == i && t.Col1 == item.Value).FirstOrDefault();
                                if (pictureAnchor != null)
                                {
                                    string imageBase64 = FastNpoiUtils.ByteToBase64(pictureAnchor.Data, pictureAnchor.FileExtention);
                                    DataOperation.SetValue(val, value.Item1, imageBase64);
                                }
                            }
                            else
                            {
                                ICell cell = row.GetCell(item.Value);
                                if (cell == null)
                                {
                                    continue;
                                }

                                object cellValue = FastNpoiUtils.GetCellValue(cell);
                                if (Cell2Prop.TryGetValue(value.Item1.Name, out Dictionary<string, object> map))
                                {
                                    if (map.TryGetValue(cellValue.ToString(), out object newValue))
                                    {
                                        cellValue = newValue;
                                    }
                                }
                                DataOperation.SetValue(val, value.Item1, cellValue);
                            }

                        }
                        catch
                        {
                            throw new Exception($"{num + 1}行数据的{item.Key}列赋值失败!");
                        }
                    }
                }

                foreach (Action<T> action in ActionList)
                {
                    action(val);
                }

                list.Add(val);
            }

            return list;
        }

        public IFastSheet<T> SetHeaderHeight(short height)
        {
            this.HeaderHeight = height;
            return this;
        }

        public IFastSheet<T> SetDataHeight(short height)
        {
            this.DataHeight = height;
            return this;
        }

        public IFastSheet<T> SetIgnoreProperty(Expression<Func<T, object>> propertyExpression)
        {
            string propertyName = FastNpoiUtils.GetExpPropertyName(propertyExpression.Body)?.ToString();
            PropertyInfo property = typeof(T).GetProperty(propertyName);
            IgnorePropertyinfos.Add(property);
            return this;
        }

        public IFastSheet<T> SetIgnoreProperty(string propertyName)
        {
            PropertyInfo property = typeof(T).GetProperty(propertyName);
            IgnorePropertyinfos.Add(property);
            return this;
        }

        public IFastSheet<T> SetProperty2CellValue<TResult>(Expression<Func<T, TResult>> expression, Dictionary<TResult, object> dictionary)
        {
            string propertyName = FastNpoiUtils.GetExpPropertyName(expression.Body)?.ToString();
            Dictionary<object, object> map = new Dictionary<object, object>();
            foreach (var entry in dictionary)
            {
                map.Add(entry.Key, entry.Value);
            }
            Prop2Cell.Add(propertyName, map);
            return this;
        }

        public IFastSheet<T> SetProperty2CellValue(string propertyName, Dictionary<object, object> dictionary)
        {
            Prop2Cell.Add(propertyName, dictionary);
            return this;
        }

        public IFastSheet<T> SetCellValue2Property<TResult>(Expression<Func<T, TResult>> expression, Dictionary<string, TResult> dictionary)
        {
            string propertyName = FastNpoiUtils.GetExpPropertyName(expression.Body)?.ToString();
            Dictionary<string, object> map = new Dictionary<string, object>();
            foreach (var entry in dictionary)
            {
                map.Add(entry.Key, entry.Value);
            }
            Cell2Prop.Add(propertyName, map);
            return this;
        }

        public IFastSheet<T> SetCellValue2Property(string propertyName, Dictionary<string, object> dictionary)
        {
            Cell2Prop.Add(propertyName, dictionary);
            return this;
        }

        public IFastSheet<T> AutoSizeColumn(bool autoSizeColumn = true)
        {
            AutoSize = autoSizeColumn;
            return this;
        }
    }
}

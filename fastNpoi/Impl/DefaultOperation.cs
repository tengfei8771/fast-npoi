﻿using fastNpoi.Interface;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Impl
{
    public class DefaultOperation : IDataOperation
    {
        
        public void SetValue<T>(T data, PropertyInfo property, object value) where T : class
        {
            Type t = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
            object SafeValue = (value == null) ? null : Convert.ChangeType(value, t);
            property.SetValue(data, SafeValue, null);
        }

        public object GetValue<T>(T data, PropertyInfo property) where T : class
        {
            return property.GetValue(data);
        }
    }
}

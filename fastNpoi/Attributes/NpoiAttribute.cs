﻿using fastNpoi.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Attributes
{
    /// <summary>
    /// excel字段特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NpoiAttribute : Attribute
    {
        /// <summary>
        /// 映射的字段名称
        /// </summary>
        public string HeaderName { get; }
        /// <summary>
        /// 表格宽度
        /// </summary>
        public int ColumnWidth { get; }
        /// <summary>
        /// 字段位置（越大越靠下,默认999）
        /// </summary>
        public int Position { get; }
        /// <summary>
        /// 表头生效条件(默认导入导出都生效,表头生效指读取时读取这列,导出时生成这列)
        /// </summary>
        public OperationType OperationType { get; }
        /// <summary>
        /// 是否合并单元格
        /// </summary>
        public bool MergeCell { get; }
        /// <summary>
        /// 单元格类型
        /// </summary>
        public DataType DataType { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="headerName">表头名称</param>
        /// <param name="columnWidth">列宽</param>
        /// <param name="position">排序字段</param>
        /// <param name="operationType">表头类型</param>
        /// <param name="mergeCell">上下相邻单元格数据相同是否合并</param>
        /// <param name="dataType">数据类型</param>
        public NpoiAttribute(
            string headerName,
            int columnWidth = 0,
            int position = 999,
            OperationType operationType = OperationType.Import_Export,
            bool mergeCell = false,
            DataType dataType = DataType.Text
            )
        {
            HeaderName = headerName;
            ColumnWidth = columnWidth;
            Position = position;
            OperationType = operationType;
            MergeCell = mergeCell;
            DataType = dataType;
        }
    }
}

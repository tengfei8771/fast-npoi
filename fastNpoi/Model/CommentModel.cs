﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Model
{
    /// <summary>
    /// excel添加注解模型
    /// </summary>
    public class CommentModel
    {
        public string Message { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
    }
}

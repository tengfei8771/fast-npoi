﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fastNpoi.Model
{
    public class PictureAnchor
    {
        public int Row1 { get;}
        public int Col1 { get;}
        public int Row2 { get;}
        public int Col2 { get;}
        public byte[] Data { get; }
        public string FileExtention { get;}
        public PictureAnchor(int row1, int col1, int row2, int col2, byte[] data, string fileExtention)
        {
            Row1 = row1;
            Col1 = col1;
            Row2 = row2;
            Col2 = col2;
            Data = data;
            FileExtention = fileExtention;
        }
    }
}

﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Text;

namespace fastNpoi.Model.Enum
{
    /// <summary>
    /// 数据类型枚举
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// 字符串类型
        /// </summary>
        Text,
        /// <summary>
        /// 数值类型 
        /// </summary>
        Number,
        /// <summary>
        /// 公式类型
        /// </summary>
        Formula,
        /// <summary>
        /// 图片类型
        /// </summary>
        Image
    }
}

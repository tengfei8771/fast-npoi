﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fastNpoi.Model.Enum
{
    public enum OperationType
    {
        Import,
        Export,
        Import_Export
    }
}

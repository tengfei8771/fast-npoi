###  **fastNpoi** 


#### 介绍
对npoi.core进行二次封装,目的是调用更加方便
(此文档是2.0版本的 1.x版本请看之前的老文档)
代码地址 https://gitee.com/tengfei8771/fast-npoi 
2.0分支


#### 安装教程

1.nuget 控制台输入

```
Install-Package fastNpoi -Version 2.0.0
```

2.nuget 管理程序 搜索fastNpoi
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20220708202359.png)


#### 使用说明

1.  依赖注入

```
serices.AddScoped(typeof(IFastNpoi), typeof(FastNpoi));
```

2.  非依赖注入

```
new FastNpoi()
```

3.  实体特性声明

```
public class TestModel
    {
        [Npoi(headerName:"主键",mergeCell:false,position:2)]
        public int Id { get; set; }
        [Npoi(headerName: "名称",position:1)]
        public string? Name { get; set; }
        [Npoi(headerName: "随机数")]
        public int RandomNum { get; set; }
        [Npoi(headerName: "创建日期")]
        public DateTime CreateTime { get; set; } = DateTime.Now;
        [Npoi(headerName: "图片",dataType:DataType.Image,columnWidth:30*256)]
        public string Image { get; set; }
        [Npoi(headerName:"公式列",dataType:DataType.Formula)]
        public string forumal { get; set; }
    }
```
headerName:对应的表头名称;
mergeCell:本条数据和下一条数据的值相同，合并单元格，默认false;
position:表头位置，默认999，数值越大越靠后;
operationType：表头类型分为导入、导出以及导入导出;
width:单元格长度
dataType:数据类型,分为text(文本类型),Number(数值类型),Formula(公式类型),Image(图片类型) ps:图片类型数据无论导入和导出,都需要定义为string类型，获取的数据为base64字符串。

4.  动态表头和实体的对应关系

```
var easyNpoi = serviceProvider.GetService<IFastNpoi>();
easyNpoi.InitWorkbook(@"C:\Users\53205\Desktop\test\20220708124735.xlsx").GetSheet<TestModel>()
.HeaderMapper("id",t=>t.Id)//通过表达式声明属性
.HeaderMapper("id","Id")//通过属性的字符串声明
.SetIgnoreProperty(t=>t.Name)；//声明忽略Name属性列(如果name属性有标记特性依旧忽略)
```
用代码声明的表头映射优先级大于特性标注

5.  简单的导入

```
var easyNpoi = serviceProvider.GetService<IFastNpoi<TestModel>>();
var list1 = easyNpoi.InitWorkbook(@"C:\Users\53205\Desktop\test\20220708124735.xlsx").GetSheet().ToList();
```
6.  简单的导出

```
var list=new List<TestModel>(1000);
for(int i=0;i<1000; i++)
{
    TestModel item = new TestModel()
    {
        Id = i,
        Name = $"创建用的名称{i}",
        RandomNum = new Random().Next()
    };
    list.Add(item);
}
var easyNpoi = serviceProvider.GetService<IFastNpoi>();
easyNpoi.CreateWorkbook().CreateSheet<TestModel>()
    .SetDataList(list)
    .ToFile(@"C:\Users\53205\Desktop\test");
```
7.设置表头样式

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>().SetHeaderStyle(workbook =>
{
    var style= workbook.CreateCellStyle();
    var font = workbook.CreateFont();
    font.FontName = "宋体";
    font.FontHeight = 15;
    font.FontHeightInPoints = 12;
    font.Color = NPOI.HSSF.Util.HSSFColor.Blue.Index;
    style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
    style.FillPattern = FillPattern.SolidForeground;
    style.SetFont(font);
    return style;
});
```
8. 设置数据样式

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>().SetDataStyle(workbook =>
{
    var style= workbook.CreateCellStyle();
    var font = workbook.CreateFont();
    font.FontName = "宋体";
    font.FontHeight = 15;
    font.FontHeightInPoints = 12;
    font.Color = NPOI.HSSF.Util.HSSFColor.Blue.Index;
    style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
    style.FillPattern = FillPattern.SolidForeground;
    style.SetFont(font);
    return style;
});
```
9.多数据源，导入同一个workbook

```
easyNpoi.CreateWorkbook()
.CreateSheet<TestModel>()
.SetDataList(list1)
.WriteToSheet()
.CreateSheet<TestModel1>()
.SetDataList(list2)
.WriteToSheet()
.ToByte();//list1和list2的元素为TestModel和TestModel1,执行之后将会把数据写入不同的shet中
```
10.数据修改 一般用于实体内存在字典映射项

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>().DataMapper(t=>t.Id=0)//相当于foreach
```
11.各种漂移量设置

```
easyNpoi.CreateWorkbook().CreateSheet()
.SetHeaderIndex(0)//表头所在行的索引 默认为0
.SetColumnStartIndex(0)//第一列所在的索引 默认为0
.SetHeaderSkipNumber(1)//表头和数据列之间的间隔 默认为1
.SetRowSkipNumber(1)//每一行之间的间隔 默认为1
.SetColumnSkipNumber(1)//每一列之间建的间隔 默认为1
```
12.手动合并单元格(原生api,做了简单封装)

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>().MergeCell(int startRowIndex,int endRowIndex,int startColumnIndex,int endColumnIndex);
```

13.生成标题列

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>()
    .SetHeaderIndex(1)//表头向下偏移1行
    .SetDataList(list)
    .SheetMapper(sheet =>
        {
            //对sheet进行追加操作,在第一行第一列加入标题数据，如果还需要别的样式,在这个action里面调用原生Api操作即可
            var row = sheet.CreateRow(0);
            var cell = row.CreateCell(0);
            cell.SetCellValue("测试的标题");
        })
    .MergeCell(0, 0, 0, 4)//第一行的4个列合并成一个单元格
    .WriteToSheet()
    .ToFile(@"C:\Users\53205\Desktop\test");
```
14.对单元格添加批注

```
easyNpoi.CreateWorkbook().CreateSheet().
    .SetCommentVisable(true)//生成的批注默认显示
    .CommentCell(new List<CommentModel>()
    {
        // 声明批注的具体位置
        new CommentModel(){RowIndex=4,ColumnIndex=0,Message="测试一个批注"},
        new CommentModel(){RowIndex=4,ColumnIndex=0,Message="测试一个批注2"},
    })
    .WriteToSheet()
    .ToFile(@"C:\Users\53205\Desktop\test");
```

15.取值和赋值实现类替换（默认实现使用反射实现,可自行替换成emit或者表达式树取值赋值提高速度）
首先实现接口

```
public interface IDataOperation
    {
        object GetValue<T>(T data, PropertyInfo property) where T : class, new();
        void SetValue<T>(T data, PropertyInfo property, object value) where T : class, new();
    }
```
然后调用

```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>().
    SetDataOperation(你的实现类)；
```

16.动态解析json对象,并将其解析成为列
首先实现接口


```
easyNpoi.CreateWorkbook().CreateSheet<TestModel>()
    .SetJsonMapping(t=>t.JsonStr,new Dictionary<string, string>()
    {
        {"json的姓名","name" },//key为列名,value为json数据内的key
        {"json的value","value" }
    })
```

17.使用excel模板导出数据(此处nuget上文档若看不见截图请进项目主页看！！)

应用
Excel模板为：(如果nuget上显示不正常请访问代码仓库查看)
![输入图片说明](excelpic.png)


| A | B |
| ------ | --|
|{{test.Id| test.Name}} |
|{template.Prop} | 表格内容 |
对象集合的声明为{{}}双括号,包裹全部需输出的属性({{和}}分别写在属性开始单元格和结束单元格内)
单个对象的声明为{}单括号(每个属性都需要包在{}内)

```
easyNpoi.InitWorkbook(@"C:\Users\tengfei8771\Desktop\excel\20220922105953.xlsx")
    .UseTemplateSheet()
    .SetData("ParamName",list[0])
    .SetDataList("listParamName", list)
    .WriteToSheet()
    .ToFile(@"C:\Users\tengfei8771\Desktop\excel");
```
```
SetData(string paramName,T data) where T:class
//单个实体数据声明,对应Excel模板字符串为{paramName.PropertyName}
```

```
SetDataList(string paramName,IEnumerable<T> dataList)
//集合数据声明,对应Excel模板字符串为{{paramName.PropertyName}}
```
18.动态类型导入导出

```
easyNpoi.CreateWorkbook()
	.CreateSheet(typeof(TestModel))//这里声明实体类型，其他的api都一样(移除所有使用表达式的接口)
        .SetDataList(list)//这个list是dynamic类型,不是泛型 但是必须为List<TestModel>
```
19.实体内部分属性值的自动映射
部分实体的字段在数据库中存储的方式是code,而非中文名称。在导入时需要将名称转化为对应的code值，导出时需要将code值转为对应的名称，可以调用如下方法进行自动映射

```
easyNpoi.CreateWorkbook()
	.CreateSheet<TestModel>()
    //此种方式指定属性值和单元格数据之间的关系，用于导出。此方法是强类型声明，推荐使用。
    .SetProperty2CellValue(t=>t.Sex,new Dictionary<int, object>() { { 0, "男" }, { 1,"女"} })
    //此种方式指定属性值和单元格数据之间的关系，用于导出。
    .SetProperty2CellValue("Sex",new Dictionary<int, object>() { { 0, "男" }, { 1,"女"} })
    //此种方式指定单元格和实体属性之间的指定关系，用于导入。此方法是强类型声明，推荐使用。
    .SetCellValue2Property(t=>t.Sex,new Dictionary<string, int>() { { "男", 0 }, { "女",1} })
    //此种方式指定单元格和实体属性之间的指定关系，用于导入
    .SetCellValue2Property("Sex",new Dictionary<string, int>() { { "男", 0 }, { "女",1} })
```
20.设置单元格自动宽度
```
easyNpoi.CreateWorkbook()
    .CreateSheet<TestModel>()
    //调用此方法即可自动在导出时根据cell内数据调整宽度，但是会忽略在实体上的宽度属性
    .AutoSizeColumn()；
```
